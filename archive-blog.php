<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri();?>/images/news/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap news-list">
    <section class="cont02" id="cont02">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/news/blog-h2.png" alt=""></h2>
      <article class="list">
        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        query_posts( array(
          'post_type'=>'blog',
          'posts_per_page' => 12,
          'paged' => $paged,
        )
      );
      ?>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php
        $terms = wp_get_object_terms($post->ID,'blog_cat');
        foreach($terms as $term){
          $cat = $term->name;
          $slug = $term->slug;
        }
        ?>
        <a href="<?php echo the_permalink();?>">
          <p class="img">
            <?php if(get_field("thumbs")):?>
              <img src="<?php the_field("thumbs");?>" alt="">
            <?php else:?>
              <img src="<?php echo get_template_directory_uri();?>/images/common/white-back.jpg" alt="">
            <?php endif;?>
          </p>
          <p class="title"><?php echo get_the_title();?></p>
          <p class="date"><?php echo get_the_date("Y.m.d");?></p>
        </a>
      <?php endwhile; else : ?>
      <?php endif; ?>
    </article>
    <?php
    if(function_exists('wp_pagenavi')) { wp_pagenavi(); }
    wp_reset_query();
    ?>
  </section>
</div>
<div class="sidebar">
  <?php get_sidebar();?>
</div>
</section>
<?php get_footer(); ?>
