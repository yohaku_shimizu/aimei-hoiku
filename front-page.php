<?php get_header(); ?>
<div class="index">
  <section class="cont01">
    <div class="wrap12">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/index/h2-01.png" alt=""></h2>
      <p class="text">
        日常の園の生活でも、給食などの衛生対策に関しても積極的に取り組み、<br class="pc">
        園の子どもたちの健やかな成長のために努めています。
      </p>
      <article class="now">
        <h3><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont01-h3.png" alt=""></h3>
        <p class="text" style="font-size:2rem;">
          <span style="font-weight: bold;color: #907758;font-size:2rem;">［2021年4月現在］</span>詳しい空き状況は園までお問い合わせください。
        </p>
        <div class="flex">
          <article class="single">
            <p class="top">
              １号認定
            </p>
            <p class="middle">
              <span>３歳児</span>
              <span>４歳児</span>
              <span>５歳児</span>
            </p>
            <p class="bottom">
              <span>空なし</span>
              <span>空なし</span>
              <span>空なし</span>
            </p>
          </article>
          <article class="single">
            <p class="top">
              2号認定
            </p>
            <p class="middle">
              <span>３歳児</span>
              <span>４歳児</span>
              <span>５歳児</span>
            </p>
            <p class="bottom">
              <span>空なし</span>
              <span>空なし</span>
              <span>若干名</span>
            </p>
          </article>
          <article class="single">
            <p class="top">
              ３号認定
            </p>
            <p class="middle">
              <span>０歳児</span>
              <span>１歳児</span>
              <span>２歳児</span>
            </p>
            <p class="bottom">
              <span>若干名</span>
              <span>空なし</span>
              <span>若干名</span>
            </p>
          </article>
        </div>
      </article>
      <article class="flex03">
        <div class="right">
          <div class="capacityimg">
            <img src="<?php echo get_template_directory_uri(); ?>/images/index/cap1.jpg" alt="">
          </div>
          <h2><img src="<?php echo get_template_directory_uri(); ?>/images/index/asobou3.jpg" alt=""></h2>
          <p class="links">
            <a href="https://reserva.be/aimei489" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/index/asobou-link1.png" alt=""></a>
            <a href="<?php echo get_template_directory_uri(); ?>/pdf/pamphlet.pdf" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/index/asobou-link2.png" alt=""></a>
          </p>
        </div>
        <div class="right">
          <div class="capacityimg">
            <img src="<?php echo get_template_directory_uri(); ?>/images/index/cap1.jpg" alt="">
          </div>
          <h2><img src="<?php echo get_template_directory_uri(); ?>/images/index/asobou-t.jpg" alt=""></h2>
          <p class="links">
            <a href="https://reserva.be/aimei489" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/index/asobou-link1.png" alt=""></a>
            <a href="<?php echo get_template_directory_uri(); ?>/pdf/asobou.pdf" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/index/asobou-link2.png" alt=""></a>
          </p>
        </div>
        <div class="right">
          <div class="capacityimg">
            <img src="<?php echo get_template_directory_uri(); ?>/images/index/cap2.jpg" alt="">
          </div>
          <h2><img src="<?php echo get_template_directory_uri(); ?>/images/index/asobou-t2.jpg" alt=""></h2>
          <p class="links">
            <a href="https://reserva.be/aimei489" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/index/asobou-link1.png" alt=""></a>
            <a href="<?php echo get_template_directory_uri(); ?>/pdf/asobou2.pdf" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/index/asobou-link2.png" alt=""></a>
          </p>
        </div>
      </article>
      <article class="flex">
        <a href="./about#cont01"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont01-img01.png" alt=""></a>
        <a href="./about#cont02"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont01-img02.png" alt=""></a>
        <a href="./about#cont03"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont01-img03.png" alt=""></a>
        <a href="./schedule#cont02"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont01-img04.png" alt=""></a>
        <a href="./schedule#cont01"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont01-img05.png" alt=""></a>
        <a href="./about#cont06"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont01-img06.png" alt=""></a>
      </article>
    </div>
  </section>
  <section class="cont02">
    <div class="wrap12">
      <article class="flex01">
        <div class="left">
          <a href="./information"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont02-img01.png" alt=""></a>
        </div>
        <div class="right">
          <img src="<?php echo get_template_directory_uri(); ?>/images/index/cont02-img02.png" alt="">
        </div>
      </article>
      <article class="flex02">
        <div class="left">
          <h3><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont02-h3-01.png" alt=""></h3>
          <article class="list">
            <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            query_posts(
              array(
                'post_type' => 'news',
                'posts_per_page' => 3,
                'paged' => $paged,
              )
            );
            ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php
                $terms = wp_get_object_terms($post->ID, 'news_cat');
                foreach ($terms as $term) {
                  $cat = $term->name;
                  $slug = $term->slug;
                }
                ?>
                <a href="<?php echo the_permalink(); ?>">
                  <span>｜ <?php echo get_the_date("Y.m.d"); ?> 　<?php echo $cat; ?></span>
                  <?php echo get_the_title(); ?>
                </a>
              <?php endwhile;
            else : ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
          </article>
          <a href="./news" class="more"><img src="<?php echo get_template_directory_uri(); ?>/images/index/more01.png" alt=""></a>
        </div>
        <div class="right">
          <h3><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont02-h3-03.png" alt=""></h3>
          <!--<article class="list">
            <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            query_posts(
              array(
                'post_type' => 'blog',
                'posts_per_page' => 3,
                'paged' => $paged,
              )
            );
            ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
              <?php
                $terms = wp_get_object_terms($post->ID, 'blog_cat');
                foreach ($terms as $term) {
                  $cat = $term->name;
                  $slug = $term->slug;
                }
              ?>
              <a href="<?php echo the_permalink(); ?>">
                <p class="left">
                  <img src="<?php the_field("thumbs"); ?>" alt="">
                </p>
                <p class="right">
                  <span>｜ <?php echo get_the_date("Y.m.d"); ?> 　<?php echo $cat; ?></span>
                  <?php echo get_the_title(); ?>
                </p>
              </a>
            <?php endwhile;
            else : ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
          </article>-->
          <p class="b-banner">
            <a href="./recruit"><img src="<?php echo get_template_directory_uri(); ?>/images/index/recruit-banner.png" alt=""></a>
          </p>
          <a href="./recruit" class="more"><img src="<?php echo get_template_directory_uri(); ?>/images/index/more03.png" alt=""></a>
        </div>
      </article>
    </div>
  </section>
  <section class="cont03">
    <div class="wrap12">
      <!-- <article class="flex01">
        <div class="left">
          <a href="./recruit"><img src="<?php //echo get_template_directory_uri(); 
                                        ?>/images/index/cont03-img01.png" alt=""></a>
        </div>
        <div class="right">
          <img src="<?php //echo get_template_directory_uri(); 
                    ?>/images/index/cont03-img02.png" alt="">
        </div>
      </article> -->
      <article class="flex02">
        <div class="left">
          <h3><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont03-h3-01.png" alt=""></h3>
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont03-img03.png" alt=""></p>
          <p class="img"><a href="./contact"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont03-img04.png" alt=""></a></p>
          <p class="img"><a href="./about/#cont07"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont03-img05.png" alt=""></a></p>
        </div>
        <div class="right">
          <h3><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont03-h3-02.png" alt=""></h3>
          <p class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3263.6957917645223!2d136.8853330152853!3d35.114306368461286!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x600379eb9a7f9bcd%3A0xec28d1530829d49!2z5oSb5ZCN5L-d6IKy5ZyS!5e0!3m2!1sja!2sjp!4v1516603962158" width="100%" height="390" frameborder="0" style="border:0" allowfullscreen></iframe>
          </p>
          <p class="img"><a href="https://www.google.com/maps?ll=35.114302,136.887522&z=16&t=m&hl=ja&gl=JP&mapclient=embed&cid=1063567584160554313"><img src="<?php echo get_template_directory_uri(); ?>/images/index/cont03-img06.png" alt=""></a></p>
        </div>
      </article>
    </div>
  </section>
</div>
<?php get_footer(); ?>