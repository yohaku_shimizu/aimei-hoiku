<?php get_header(); ?>
<?php $td = get_template_directory_uri();?>
<div class="dispa under working">
  <section class="page-title">
    <div class="wrap10">
      <h2>Dispatch<span>/講師派遣</span></h2>
    </div>
  </section>
  <section class="mv">
    <div class="wrap10">
      <p class="img"><img src="<?php echo $td;?>/images/dispa/mv.png" alt=""></p>
      <article class="flex">
        <h3>Summary<span>概要</span></h3>
        <p class="text">
          リトミック・音楽・英会話・体操・マーチングバンドなど、当社のネットワークより、ご要望にあった講師紹介や企画提案をさせていただきます。
        </p>
      </article>
    </div>
  </section>
  <section class="cont1">
    <div class="wrap128">
      <article v-for="(cont,index) in content" class="single" :class='"single" + index'>
        <div class="text-box" :style="{background:'url(<?php echo $td;?>/images/common/back' + (index + 1) +'.png) no-repeat'}">
          <h3>#0{{(index + 1)}}<span>{{cont.subtitle}}</span></h3>
          <p class="text" v-html="cont.text"></p>
        </div>
        <p class="img"><img :src="'<?php echo $td;?>/images/dispa/img' + (index + 1) +'.png'" alt=""></p>
      </article>
    </div>
  </section>
  <section class="cont2">
    <div class="wrap128">
      <article class="img">
        <p class="img"><img src="<?php echo $td;?>/images/dispa/img4.png" alt=""></p>
        <p class="img"><img src="<?php echo $td;?>/images/dispa/img5.png" alt=""></p>
      </article>
      <article class="flex">
        <div class="left">
          <h3>Works<span>講師派遣実績</span></h3>
        </div>
        <div class="right">
          <dl v-for="(list,index) in works">
            <dt>{{list.left}}</dt>
            <dd v-html="list.right"></dd>
          </dl>
        </div>
      </article>
      <a href="<?php echo get_home_url();?>/works" class="more">View More</a>
    </div>
  </section>
</div>
<?php get_footer(); ?>
