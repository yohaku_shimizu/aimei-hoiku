<?php get_header(); ?>
<div class="contact">
  <section class="cont01">
    <div class="wrap96">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/contact/h2.png" alt=""></h2>
      <article class="form">
        <?php echo do_shortcode('[contact-form-7 id="4" title="お問い合わせ"]');?>
      </article>
    </div>
  </section>
</div>
<?php get_footer(); ?>
