<?php
  $post = get_post_type();
?>
<article class="guide">
  <a href="<?php echo get_home_url();?>/information"><img src="<?php echo get_template_directory_uri()?>/images/common/side01.png" alt=""></a>
</article>
<article class="news-title">
  <img src="<?php echo get_template_directory_uri();?>/images/common/side02.png" alt="">
</article>
<article class="cat all">
  <h2>カテゴリー</h2>
  <ul>
    <?php wp_list_categories('title_li=&number=5&taxonomy=news_cat'); ?>
  </ul>
</article>
<article class="mon all">
  <h2>アーカイブ</h2>
  <ul>
    <?php wp_get_archives('title_li=&numver=5&post_type=news&type=monthly'); ?>
  </ul>
</article>
<article class="tell">
  <img src="<?php echo get_template_directory_uri();?>/images/common/side03.png" alt="">
</article>
<article class="contact">
  <a href="<?php echo get_home_url();?>/contact"><img src="<?php echo get_template_directory_uri();?>/images/common/side04.png" alt=""></a>
</article>
<article class="download">
  <a href="<?php echo get_home_url();?>/about#cont07"><img src="<?php echo get_template_directory_uri();?>/images/common/side05.png" alt=""></a>
</article>
<article class="single">
  <p class="top">
    １号認定
  </p>
  <p class="middle">
    <span>３歳児</span>
    <span>４歳児</span>
    <span>５歳児</span>
  </p>
  <p class="bottom">
    <span>空なし</span>
    <span>空なし</span>
    <span>空なし</span>
  </p>
</article>
<article class="single">
  <p class="top">
    2号認定
  </p>
  <p class="middle">
    <span>３歳児</span>
    <span>４歳児</span>
    <span>５歳児</span>
  </p>
  <p class="bottom">
    <span>空なし</span>
    <span>空なし</span>
    <span>若干名</span>
  </p>
</article>
<article class="single">
  <p class="top">
    ３号認定
  </p>
  <p class="middle">
    <span>０歳児</span>
    <span>１歳児</span>
    <span>２歳児</span>
  </p>
  <p class="bottom">
    <span>若干名</span>
    <span>空なし</span>
    <span>若干名</span>
  </p>
</article>
<article class="reserve">
  <a href="https://reserva.be/aimei489"><img src="<?php echo get_template_directory_uri();?>/images/common/side07.png" alt=""></a>
</article>
<div class="asobou asobou2">
  <h2><img src="<?php echo get_template_directory_uri();?>/images/index/asobou3.jpg" alt=""></h2>
  <p class="links">
    <a href="https://reserva.be/aimei489" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/index/asobou-link1.png" alt=""></a>
    <a href="<?php echo get_template_directory_uri();?>/pdf/pamphlet.pdf" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/index/asobou-link2.png" alt=""></a>
  </p>
</div>
<div class="asobou asobou2">
  <h2><img src="<?php echo get_template_directory_uri();?>/images/index/asobou-t.jpg" alt=""></h2>
  <p class="links">
    <a href="https://reserva.be/aimei489" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/index/asobou-link1.png" alt=""></a>
    <a href="<?php echo get_template_directory_uri();?>/pdf/asobou1-1.pdf" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/index/asobou-link2.png" alt=""></a>
  </p>
</div>
<div class="asobou">
  <h2><img src="<?php echo get_template_directory_uri();?>/images/index/asobou-t2.jpg" alt=""></h2>
  <p class="links">
    <a href="https://reserva.be/aimei489" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/index/asobou-link1.png" alt=""></a>
    <a href="<?php echo get_template_directory_uri();?>/pdf/asobou2.pdf" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/index/asobou-link2.png" alt=""></a>
  </p>
</div>
