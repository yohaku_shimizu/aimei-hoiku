<?php get_header(); ?>
<script>
  jQuery(document).ready(function(){
    location.href="http://houjin.aimei-hoiku.com/";
  });
</script>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri();?>/images/company/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap company">
    <nav class="page-nav">
      <a href="#cont01">法人情報</a>
      <a href="#cont02">各種資料</a>
    </nav>
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/company/h2-01.png" alt=""></h2>
      <article class="list2">
        <dl>
          <dt>法人名</dt>
          <dd>社会福祉法人 愛名</dd>
        </dl>
        <dl>
          <dt>理事長</dt>
          <dd>日比 裕子</dd>
        </dl>
        <dl>
          <dt>設立年月日</dt>
          <dd>平成27年４月１日</dd>
        </dl>
        <dl>
          <dt>所在地</dt>
          <dd>〒455-0001 名古屋市港区七番町五丁目３番地</dd>
        </dl>
        <dl>
          <dt>電話番号</dt>
          <dd>052-653-6016</dd>
        </dl>
        <dl>
          <dt>FAX</dt>
          <dd>052-653-6029</dd>
        </dl>
        <dl>
          <dt>運営施設</dt>
          <dd>愛名保育園（幼保連携型認定こども園）</dd>
        </dl>
      </article>
      <article class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3263.6948580077287!2d136.88553301524334!3d35.114329680330364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x600379eb8546f0fb%3A0x7219dd8b421af276!2z44CSNDU1LTAwMDEg5oSb55-l55yM5ZCN5Y-k5bGL5biC5riv5Yy65LiD55Wq55S677yV5LiB55uu77yT!5e0!3m2!1sja!2sjp!4v1518066163806" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        <a href="https://www.google.com/maps?ll=35.11433,136.887722&z=16&t=m&hl=ja&gl=JP&mapclient=embed&q=%E3%80%92455-0001+%E6%84%9B%E7%9F%A5%E7%9C%8C%E5%90%8D%E5%8F%A4%E5%B1%8B%E5%B8%82%E6%B8%AF%E5%8C%BA%E4%B8%83%E7%95%AA%E7%94%BA%EF%BC%95%E4%B8%81%E7%9B%AE%EF%BC%93">
          <img src="<?php echo get_template_directory_uri();?>/images/company/map-link.png" alt="">
        </a>
      </article>
    </section>
    <section class="cont02" id="cont02">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/company/h2-02.png" alt=""></h2>
      <p class="text">
        社会福祉法人愛名の定款・役員名簿などの資料を<br>
        PDFファイルで掲載おります。
      </p>
      <article class="block">
        <div class="flex">
          <p class="single"><a href="<?php echo get_template_directory_uri();?>/pdf/teikan.pdf" target="_blank">定　款</a></p>
          <p class="single"><a href="<?php echo get_template_directory_uri();?>/pdf/yakuin.pdf" target="_blank">役員等名簿</a></p>
          <p class="single"><a href="<?php echo get_template_directory_uri();?>/pdf/houshu.pdf" target="_blank">役員等報酬および費用弁償規程</a></p>
          <p class="single"><a href="<?php echo get_template_directory_uri();?>/pdf/zangaku.pdf" target="_blank">社会福祉充実残額算定シート</a></p>
        </div>
      </article>
    </section>
  </div>
  <div class="sidebar">
    <?php get_sidebar();?>
  </div>
</section>
<?php get_footer(); ?>
