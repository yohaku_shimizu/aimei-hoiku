<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri();?>/images/news/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap news-list">
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/news/news-h2.png" alt=""></h2>
      <article class="list">
        <?php

        // --------------------------------------------------------------------------------------------------------------
        // 2020/12/18 追加記述　清水
        // dateかどうかの条件分岐の is_month() が機能していないため全て最新一覧ページになってしまう修正
        $page_url = (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $url_explode = explode("/",$page_url);
        // dateページでないためURLから無理やりdateか判断
        // スラッシュ区切りにした際配列の要素が6以上であればdateページとする
        if (count($url_explode) > 6) {
          // スラッシュ区切りのURLを配列でループ
          foreach ($url_explode as $key => $value) {
            // newsのindexを取得
            if ($value == "news") {
              $news_index = $key;
            }
            // news の index+1 が年
            if ($key == $news_index + 1 && $news_index ) {
              $value = intval($value);
              $args['year'] = $value;
            }
            // news の index+2 が月
            if ($key == $news_index + 2 && $news_index ) {
              $value = intval($value);
              $args['monthnum'] = $value;
            }
          }
          // date判定なのでdate_queryをセット
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          query_posts(
            array(
              'post_type'=>'news',
              'posts_per_page' => 12,
              'paged' => $paged,
              'date_query' => array(
                array(
                  'year'  => $args['year'],
                  'month' => $args['monthnum'],
                ),
              ),
            )
          );
        }else {
          // date判定ではないため最新記事を取得
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          query_posts(
            array(
              'post_type'=>'news',
              'posts_per_page' => 12,
              'paged' => $paged,
            )
          );
        }

        // 元々記述してあったコード
        // if(is_month()){
        //   $setMonth=get_the_date('m');
        //   $setYear=get_the_date('Y');
        //   $args['monthnum']=$setMonth;
        //   $args['year']=$setYear;
        // }

        // $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        // query_posts(
        //   array(
        //     'post_type'=>'news',
        //     'posts_per_page' => 12,
        //     'paged' => $paged,
        //   )
        // );

        // --------------------------------------------------------------------------------------------------------------

        ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <?php
          $terms = wp_get_object_terms($post->ID,'news_cat');
          foreach($terms as $term){
            $cat = $term->name;
            $slug = $term->slug;
          }
          ?>
          <a href="<?php echo the_permalink();?>">
            <p class="tag">｜ <?php echo get_the_date("Y.m.d");?> 　<?php echo $cat;?></p>
            <p class="title"><?php echo get_the_title();?></p>
          </a>
        <?php endwhile; else : ?>
        <?php endif; ?>
        <?php
        if(function_exists('wp_pagenavi')) { wp_pagenavi(); }
        wp_reset_query();
        ?>
      </article>
    </section>
  </div>
  <div class="sidebar">
    <?php get_sidebar();?>
  </div>
</section>
<?php get_footer(); ?>
