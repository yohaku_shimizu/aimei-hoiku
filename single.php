<?php get_header(); ?>
<div class="single-topics">
  <section class="sv">
    <img src="<?php echo get_template_directory_uri()?>/images/topics/sv.png" alt="">
  </section>
  <section class="cont01">
    <div class="wrap10">
      <article class="top">
        <h2><?php echo get_the_title();?></h2>
        <p class="tag"><?php echo get_the_date("Y.m.d");?></p>
      </article>
      <article class="main-wrap">
        <?php
        $this_content= wpautop($post->post_content);
        echo $this_content;
        ?>
      </article>
    </div>
  </section>
</div>
<?php get_footer(); ?>
