<section class="green-back">
  <img src="<?php echo get_template_directory_uri();?>/images/common/tree-back.png" alt="">
</section>
<footer>
  <section class="pagetop">
    <a href="#">
      <img src="<?php echo get_template_directory_uri();?>/images/common/pagetop.png" alt="">
    </a>
  </section>
  <div class="wrap12">
    <nav class="footer-nav pc">
      <section class="single">
        <p class="big"><a href="<?php echo get_home_url();?>">ホーム</a></p>
        <p class="big"><a href="<?php echo get_home_url();?>/about">当園について</a></p>
        <ul class="small">
          <li><a href="<?php echo get_home_url();?>/about#cont01">ごあいさつ</a></li>
          <li><a href="<?php echo get_home_url();?>/about#cont02">園の特徴</a></li>
          <li><a href="<?php echo get_home_url();?>/about#cont03">園の目標</a></li>
          <li><a href="<?php echo get_home_url();?>/about#cont04">目指す子ども像</a></li>
          <li><a href="<?php echo get_home_url();?>/about#cont05">園の歴史</a></li>
          <li><a href="<?php echo get_home_url();?>/about#cont06">施設概要</a></li>
          <li><a href="<?php echo get_home_url();?>/about#cont07">各種資料のダウンロード</a></li>
        </ul>
      </section>
      <section class="single">
        <p class="big"><a href="<?php echo get_home_url();?>/schedule">園での生活</a></p>
        <ul class="small">
          <li><a href="<?php echo get_home_url();?>/schedule#cont01">一日の様子</a></li>
          <li><a href="<?php echo get_home_url();?>/schedule#cont02">年間行事・月間行事</a></li>
        </ul>
        <p class="big"><a href="<?php echo get_home_url();?>/information">入園案内</a></p>
        <ul class="small">
          <li><a href="<?php echo get_home_url();?>/information#cont01">入園までの流れ</a></li>
          <li><a href="<?php echo get_home_url();?>/information#cont02">入園資料のダウンロード</a></li>
        </ul>
        <p class="big"><a href="<?php echo get_home_url();?>/news">最新情報</a></p>
      </section>
      <section class="single">
        <p class="big"><a href="<?php echo get_home_url();?>/recruit">採用情報</a></p>
        <ul class="small">
          <li><a href="<?php echo get_home_url();?>/recruit#cont01">メッセージ</a></li>
          <li><a href="<?php echo get_home_url();?>/recruit#cont03">募集要項・見学情報</a></li>
          <li><a href="<?php echo get_home_url();?>/recruit#cont04">採用お問い合わせ</a></li>
        </ul>
        <p class="big"><a href="<?php echo get_home_url();?>/contact">お問い合わせ</a></p>
        <p class="big"><a href="https://reserva.be/donko489">オンライン予約</a></p>
        <p class="big"><a href="http://houjin.aimei-hoiku.com">社会福祉法人愛名</a></p>
        <p class="big"><a href="http://aimei-hoiku.com">あいめい保育園</a></p>
      </section>
      <section class="single mail-post">
        <p class="big">POST</p>
        <p class="text">
          保育園へのご意見・ご提案等ございました
          ら、どうぞご遠慮なくお申し出ください。
        </p>
        <?php echo do_shortcode('[contact-form-7 id="32" title="ポスト"]');?>
      </section>
    </nav>
    <section class="info">
      <article class="logo">
        <a href="<?php echo get_home_url();?>"><img src="<?php echo get_template_directory_uri();?>/images/common/footer-logo.png" alt=""></a>
      </article>
      <article class="info-wrap">
        <p class="text">所在地： 〒455-0068 名古屋市港区土古町2丁目23番地</p>
        <p class="text">TEL：052-389-5132</p>
        <p class="text">E-mail:donko@aimei-hoiku.com</p>
        <p class="copy">
          Copyright &copy; aimei hoiku All rights reserved.
        </p>
      </article>
    </section>
  </div>
</footer>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/slick.js"></script>
<script type="text/javascript">
<?php $slug = get_post($wp_query->post->ID)->post_name; ?>
$(window).on("load resize",function(){
  var ww = $(this).width();
  if(ww >= 1200){
    $(".t-button").removeClass('t-button-act');
    $(".nav-wrap").removeClass('nav-wrap-act');
  }
});
$(".t-button").click(function(){
  $(".t-button").toggleClass('t-button-act');
  $(".nav-wrap").toggleClass('nav-wrap-act');
});
$(".commingsoon").click(function(){
  alert('現在公開されておりません。掲載までもうしばらくお待ちください');
  return false;
});
$(function(){
  $(".mv").slick({
    centerMode: false,
    slidesToShow: 1,
    dots: true,
    arrows:false,
    respondTo:'slider',
    autoplay:true,
    autoplaySpeed:3000,
    fade:true,
    speed:1500,
    pauseOnHover:false
  });
});
<?php if($slug == "schedule"):?>
$(function(){
  $("ul.slide").slick({
    centerMode: false,
    slidesToShow: 1,
    dots: true,
    arrows:false,
    respondTo:'slider',
    autoplay:true,
    autoplaySpeed:3000,
    fade:true,
    speed:1500,
  });
});
<?php endif;?>
$(window).on("load resize scroll",function(){
  var ww = $(".mv").width() / 2;
  var bs = $("button.slick-prev").width() / 2;
  var rp = ww - 590 - bs;
  var lp = rp;
  if(ww >= 600){
    $(".slick-prev").css("left",lp);
    $(".slick-next").css("right",rp);
  }else{
    $(".slick-prev").css("left","0");
    $(".slick-next").css("right","0");
  }
  var st = $(window).scrollTop();
  var fp = $("nav.header").innerHeight() + $("nav.header").offset().top;
  if(st > fp){
    $("section.fixed-nav").addClass('fixed-nav-in');
  }else{
    $("section.fixed-nav").removeClass('fixed-nav-in');
  }
});
$(document).ready(function(){
  $("section.mv div.img").each(function(){
    var Img = "url('" +$(this).children("img").attr("src") + "')";
    $(this).css("background-image",Img);
  });
});
$("section.pagetop a").on("click",function(){
  $("body,html").animate({scrollTop:0}, 1000);
  return false;
});
</script>
<?php wp_footer(); ?>
</body>
</html>
