<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri();?>/images/about/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap about">
    <section class="midashi">
      <p class="img"><img src="<?php echo get_template_directory_uri();?>/images/about/h2-01.png" alt=""></p>
    </section>
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/about/h2-02.png" alt=""></h2>
      <p class="img">
        <img src="<?php echo get_template_directory_uri();?>/images/about/img01.png" alt="">
      </p>
      <p class="text">
        名古屋市土古保育園から民間移管し、<br class="pc">
        2019年４月１日、新たに「土古おおぞら保育園」として開園します。<br class="pc">
        引き継ぎました、子ども一人ひとりを大切にし、<br class="pc">
        子ども、保護者、職員が共に育ち合う保育園を目指します。
      </p>
    </section>
    <nav class="page-nav">
      <a href="#cont02">園の特徴</a>
      <a href="#cont03">園の目標</a>
      <a href="#cont04">目指す子ども像</a>
      <a href="#cont05">園の歴史</a>
      <a href="#cont06">施設概要</a>
      <a href="#cont07">各種資料</a>
    </nav>
    <section class="cont02" id="cont02">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/about/h2-03.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri();?>/images/about/img02.png" alt=""></p>
      <p class="text">
        2019年4月より民間移管し、開園する保育園です。<br>
        90人定員の家庭的な雰囲気の中で子ども1人ひとりの<br>
        成長にあわせた保育をしています。<br>
        思い切り体を動かして遊べる園庭やプール、野菜の栽培や<br>
        お花を楽しめる畑もあります。
      </p>
    </section>
    <section class="cont03" id="cont03">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/about/h2-04.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri();?>/images/about/img03.png" alt=""></p>
      <p class="text">
        身近な物事に感謝する心を育てる。<br>
        人との関わりの中で愛情と信頼感を育み、助け合える子を育てる。<br>
        家庭的な雰囲気の中で、子どものさまざまな思いを受けとめ、情緒の安定を図る。<br>
        健康・安全など、生活に必要な基本的な習慣や態度を養い、心身の健康の基礎を身につける。<br>
        身の回りのことについて興味関心を育て、思考力の基礎や創造性の芽生えを培う。<br>
        環境を通して、遊びの中から生きる力の基礎となる自己肯定感(自尊感情)を養う。
      </p>
    </section>
    <section class="cont04" id="cont04">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/about/h2-05.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri();?>/images/about/img04.png" alt=""></p>
      <p class="text">
        子ども1人ひとりを大切に、子ども、保護者、職員が共に<br>
        育ち合う保育園を目指します。<br>
        仲間と一緒に遊ぶ経験を通して自分も周りの人も<br>
        大切にする優しさが育つようにしていきます。
      </p>
    </section>
    <section class="cont05" id="cont05">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/about/h2-06.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri();?>/images/about/img05.png" alt=""></p>
      <article class="list">
        <dl>
          <dt>昭和２８年５月１日</dt>
          <dd>
            開園　名称「愛名保育園」<br>
            園長　日比 高則就任
          </dd>
        </dl>
        <dl>
          <dt>昭和２８年７月１日</dt>
          <dd>社会福祉施設として認可を受ける（定員６０名）</dd>
        </dl>
        <dl>
          <dt>昭和３０年９月１日</dt>
          <dd>日比 静子　愛名保育園長就任</dd>
        </dl>
        <dl>
          <dt>昭和３７年３月</dt>
          <dd>園舎全面改築完成</dd>
        </dl>
        <dl>
          <dt>昭和４６年４月１日</dt>
          <dd>定員８０名に変更</dd>
        </dl>
        <dl>
          <dt>昭和４８年６月６日</dt>
          <dd>中山 満寿子　愛名保育園長就任</dd>
        </dl>
        <dl>
          <dt>昭和５０年４月１日</dt>
          <dd>障がい児保育指定園となる</dd>
        </dl>
        <dl>
          <dt>昭和５７年４月１日</dt>
          <dd>乳児保育の開始とともに定員９０名に変更</dd>
        </dl>
        <dl>
          <dt>平成２７年４月１日</dt>
          <dd>
            社会福祉法人愛名設立　理事長　日比 裕子就任<br>
            愛名保育園長　日比 勇三就任
          </dd>
        </dl>
        <dl>
          <dt>平成２９年９月１日</dt>
          <dd>
            名古屋市土古保育園を引継ぐ法人に選定される
          </dd>
        </dl>
        <dl>
          <dt>平成３０年３月</dt>
          <dd>愛名保育園新園舎全面完成</dd>
        </dl>
        <dl>
          <dt>平成３０年４月１日</dt>
          <dd>愛名保育園は幼保認定連携型認定こども園に移行<br>名古屋市土古保育園において引継ぎ共同保育開始</dd>
        </dl>
        <dl>
          <dt>平成３１年４月１日</dt>
          <dd>「土古おおぞら保育園」開園。園長　吉留 月子就任</dd>
        </dl>
      </article>
    </section>
    <section class="cont06" id="cont06">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/about/h2-07.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri();?>/images/about/img06.png" alt=""></p>
      <article class="list2">
        <dl>
          <dt>園名</dt>
          <dd>土古おおぞら保育園</dd>
        </dl>
        <dl>
          <dt>所在地</dt>
          <dd>〒455-0068 名古屋市港区土古町2丁目23番地</dd>
        </dl>
        <dl>
          <dt>連絡先</dt>
          <dd>TEL：052-389-5132</dd>
        </dl>
        <dl>
          <dt>最寄駅</dt>
          <dd>
            あおなみ線「名古屋競馬場前」駅 徒歩７分<br>
            名古屋市営バス「土古」停 徒歩５分
          </dd>
        </dl>
        <dl>
          <dt>受け入れ年齢</dt>
          <dd>
            6ヵ月から小学校就学前まで
          </dd>
        </dl>
        <dl>
          <dt>開所日</dt>
          <dd>
            月曜日から土曜日まで
          </dd>
        </dl>
        <dl>
          <dt>開所時間</dt>
          <dd>７：３０～１９：３０</dd>
        </dl>
        <dl>
          <dt>短時間保育</dt>
          <dd>８：３０～１６：３０</dd>
        </dl>
        <dl>
          <dt>延長保育</dt>
          <dd>１８：３０～１９：３０</dd>
        </dl>
        <dl>
          <dt>土曜日</dt>
          <dd>７：３０～１９：３０</dd>
        </dl>
        <dl>
          <dt>休所日</dt>
          <dd>日曜日、国民の祝日、休日<br>＊12月29日から1月3日は休園日となります</dd>
        </dl>
      </article>
      <article class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3263.8428825051415!2d136.85898661524337!3d35.110633980331265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6003782daf0985fb%3A0x59748a68a305b44c!2z5Zyf5Y-k5L-d6IKy5ZyS!5e0!3m2!1sja!2sjp!4v1547538503267" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        <a href="https://goo.gl/maps/bWxLwtBaQ852">
          <img src="<?php echo get_template_directory_uri();?>/images/about/map-link.png" alt="">
        </a>
      </article>
    </section>
    <section class="cont07" id="cont07">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/about/h2-08.png" alt=""></h2>
      <p class="text">
        様々な提出書類書式をダウンロード・プリントアウトして提出できます。<br>
        どうぞご活用ください。
      </p>
      <article class="block">
        <div class="flex">
          <p class="left">
            お医者さんから処方された薬を<span>やむを得ず</span>持参される場合は、お薬表を印刷し必要事項を記入の上、薬と一緒に直接職員にお渡しください。
          </p>
          <div class="right">
            <a href="<?php echo get_template_directory_uri();?>/pdf/kusuri.pdf" target="_blank">お薬表</a>
            <a href="<?php echo get_template_directory_uri();?>/pdf/kusuri-kana.pdf" target="_blank">おくすりひょう（ルビあり）</a>
          </div>
        </div>
      </article>
    </section>
  </div>
  <div class="sidebar">
    <?php get_sidebar();?>
  </div>
</section>
<?php get_footer(); ?>
