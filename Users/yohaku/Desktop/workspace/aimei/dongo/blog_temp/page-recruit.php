<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri();?>/images/recruit/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap recruit">
    <nav class="page-nav">
      <a href="#cont01">メッセージ</a>
      <!--<a href="#cont02">先輩の声</a>-->
      <a href="#cont03">募集要項</a>
      <a href="#cont04">お問い合わせ</a>
      <a href="" class="dammy" style="opacity:0;"></a>
    </nav>
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/recruit/h2-01.png" alt=""></h2>
      <p class="img">
        <img src="<?php echo get_template_directory_uri();?>/images/recruit/img01.png" alt="">
      </p>
      <p class="text">
        土古おおぞら保育園はあたたかい言葉が響き合う環境の元で子ども達がのびのびと生活しています。小規模な保育園なので、クラスの仲はもちろんのこと、異年齢の友達との関わりも密にもつことができ、保育士と子ども1人ひとりが、より深く向き合って関係を築くことができます。また、広い園庭や近くに公園がいくつもあるので、天気の良い日は毎日外に出て、元気に遊ぶことができます。<br>
        外国籍の家庭も多く、いろいろな国の文化に触れる機会ができたり、子ども達が1人ひとりの個性を認め合い、助け合いながら成長しています。<br>
        職員間のコミュニケーションも大切にしており、情報を伝え合ったり、わからないことや困ったことを相談し合えたり、保育士が仲良く笑顔で過ごすことで、子ども達も楽しいと思える雰囲気になるよう心がけています。
      </p>
    </section>
    <!--<section class="cont02" id="cont02">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/recruit/h2-02.png" alt=""></h2>
      <article class="block">
        <p class="img"><img src="<?php echo get_template_directory_uri();?>/images/recruit/test-img.png" alt=""></p>
        <p class="text">
          メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文
        </p>
      </article>
      <article class="block">
        <p class="img"><img src="<?php echo get_template_directory_uri();?>/images/recruit/test-img.png" alt=""></p>
        <p class="text">
          メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文メッセージ本文
        </p>
      </article>
    </section>-->
    <section class="cont03" id="cont03">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/recruit/h2-02.png" alt=""></h2>
      <article class="list">
        <dl>
          <dt>職種</dt>
          <dd>保育士（オープニングスタッフ）</dd>
        </dl>
        <dl>
          <dt>雇用形態</dt>
          <dd>正社員</dd>
        </dl>
        <dl>
          <dt>募集人数</dt>
          <dd>2人</dd>
        </dl>
        <dl>
          <dt>応募資格</dt>
          <dd>
            学歴: 短大以上
          </dd>
        </dl>
        <dl>
          <dt>免許・資格</dt>
          <dd>
            保育士免許
          </dd>
        </dl>
        <dl>
          <dt>仕事内容</dt>
          <dd>
            ◆保育士業務<br>
            ・2019年4月より民間移管され開園する保育園です。<br>
            ・2019年4月1日からの採用、就業となります。<br>
            ・スタッフは10名の予定
          </dd>
        </dl>
        <dl>
          <dt>勤務地</dt>
          <dd>
            〒455-0068<br>
            名古屋市港区土古町2丁目23番地<br>
            土古おおぞら保育園
          </dd>
        </dl>
        <dl>
          <dt>勤務時間</dt>
          <dd>
            月の変形労働時間制<br>
            【平日】<br>
            7:30〜16:15（休憩45分）<br>
            7:45〜16:30（休憩45分）<br>
            8:30〜17:15（休憩45分）<br>
            9:00〜18:00（休憩1時間）<br>
            9:45〜19:30（休憩1時間）<br>
            【土曜日】<br>
            7:30〜16:15（休憩45分）<br>
            10:45〜19:30（休憩45分）
          </dd>
        </dl>
        <!--<dl>
          <dt>給与・賞与</dt>
          <dd>大卒：228,440円<br>短大：198,605円<br>年２回</dd>
        </dl>-->
        <dl>
          <dt>手当</dt>
          <dd>通勤手当：実費<br></dd>
        </dl>
        <dl>
          <dt>社会保険</dt>
          <dd>健康保険・厚生年金保険・雇用保険・労災保険・退職金制度</dd>
        </dl>
        <dl>
          <dt>選考日</dt>
          <dd>
            1月7日　以降随時
          </dd>
        </dl>
        <dl>
          <dt>備考</dt>
          <dd>
            平成31年3月（スタート日は応相談）から就業していただきます。（使用期間は時給900円・手当なし）<br>
            制服なし
          </dd>
        </dl>
      </article>
      <article class="list">
        <dl>
          <dt>職種</dt>
          <dd>保育補助（オープニングスタッフ）</dd>
        </dl>
        <dl>
          <dt>雇用形態</dt>
          <dd>パート労働者</dd>
        </dl>
        <dl>
          <dt>募集人数</dt>
          <dd>2人</dd>
        </dl>
        <dl>
          <dt>仕事内容</dt>
          <dd>
            ◆保育補助業務<br>
            ・2019年4月より民間移管され開園する保育園です。<br>
            ・2019年4月1日からの採用、就業となります。<br>
            ・スタッフは10名の予定
          </dd>
        </dl>
        <dl>
          <dt>勤務地</dt>
          <dd>
            〒455-0068<br>
            名古屋市港区土古町2丁目23番地<br>
            土古おおぞら保育園
          </dd>
        </dl>
        <dl>
          <dt>勤務時間</dt>
          <dd>
            16:00〜19:30<br>
            週5日〜6日
          </dd>
        </dl>
        <dl>
          <dt>給与</dt>
          <dd>時給：1,000円〜1,300円</dd>
        </dl>
        <dl>
          <dt>備考</dt>
          <dd>
            制服なし（エプロンの準備をお願いします）
          </dd>
        </dl>
      </article>
    </section>
    <section class="cont04" id="cont04">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/recruit/h2-03.png" alt=""></h2>
      <article class="form">
        <?php echo do_shortcode('[contact-form-7 id="28" title="採用"]');?>
      </article>
    </section>
  </div>
  <div class="sidebar">
    <?php get_sidebar();?>
  </div>
</section>
<?php get_footer(); ?>
