<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="format-detection" content="telephone=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>土古おおぞら保育園 | 社会福祉法人愛名</title>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/ress.css">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/slick.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/slick-theme.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/common.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/animate.min.css">
  <?php wp_head(); ?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120892141-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120892141-1');
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-YjutKSuKYyNXhSps50eP8fk0nFoxgEE"></script>
</head>
<body>
  <section class="t-button tab">
    <div class="bar bar1"></div>
    <div class="bar bar2"></div>
    <div class="bar bar3"></div>
  </section>
  <section class="nav-wrap">
    <nav class="rgl">
      <p><a href="<?php echo get_home_url();?>/">ホーム</a></p>
      <p><a href="<?php echo get_home_url();?>/about">当園について</a></p>
      <p><a href="<?php echo get_home_url();?>/schedule">園での生活</a></p>
      <p><a href="<?php echo get_home_url();?>/information">入園案内</a></p>
      <p><a href="<?php echo get_home_url();?>/news">最新情報</a></p>
      <p><a href="<?php echo get_home_url();?>/recruit">採用情報</a></p>
      <p><a href="<?php echo get_home_url();?>/contact">お問い合わせ</a></p>
      <p><a href="http://houjin.aimei-hoiku.com">社会福祉法人愛名→</a></p>
      <p><a href="http://aimei-hoiku.com">あいめい保育園→</a></p>
      <p><a href="https://reserva.be/donko489">オンライン予約</a></p>
    </nav>
  </section>
  <section class="fixed-nav pc2">
    <div class="wrap12 flex">
      <nav class="left">
        <a href="<?php echo get_home_url();?>/about">当園について<span>about</span></a>
        <a href="<?php echo get_home_url();?>/schedule">園の生活<span>schedule</span></a>
        <a href="<?php echo get_home_url();?>/information">入園案内<span>information</span></a>
      </nav>
      <article class="logo">
        <a href="<?php echo get_home_url();?>/"><img src="<?php echo get_template_directory_uri();?>/images/common/logo-icon.png" alt=""></a>
      </article>
      <nav class="right">
        <a href="<?php echo get_home_url();?>/news">最新情報<span>news</span></a>
        <a href="<?php echo get_home_url();?>/recruit">採用情報<span>recruit</span></a>
        <a href="<?php echo get_home_url();?>/contact">お問い合わせ<span>contact</span></a>
      </nav>
    </div>
  </section>
  <header>
    <div class="wrap12">
      <article class="kanban pc2">
        <img src="<?php echo get_template_directory_uri();?>/images/common/kanban.png" alt="">
      </article>
      <nav class="atoduke pc2">
        <ul>
          <li><a href="http://houjin.aimei-hoiku.com">社会福祉法人愛名→</a></li>
          <li><a href="http://aimei-hoiku.com">あいめい保育園→</a></li>
        </ul>
        <p><a href="https://reserva.be/donko489">オンライン予約</a></p>
      </nav>
      <h1 class="logo">
        <a href="<?php echo get_home_url();?>"><img src="<?php echo get_template_directory_uri();?>/images/common/header-logo.png" alt=""></a>
      </h1>
      <nav class="header pc2">
        <a href="<?php echo get_home_url();?>/"><img src="<?php echo get_template_directory_uri();?>/images/common/nav01.png" alt=""></a>
        <a href="<?php echo get_home_url();?>/about"><img src="<?php echo get_template_directory_uri();?>/images/common/nav02.png" alt=""></a>
        <a href="<?php echo get_home_url();?>/schedule"><img src="<?php echo get_template_directory_uri();?>/images/common/nav03.png" alt=""></a>
        <a href="<?php echo get_home_url();?>/information"><img src="<?php echo get_template_directory_uri();?>/images/common/nav04.png" alt=""></a>
        <a href="<?php echo get_home_url();?>/news"><img src="<?php echo get_template_directory_uri();?>/images/common/nav05.png" alt=""></a>
        <a href="<?php echo get_home_url();?>/recruit"><img src="<?php echo get_template_directory_uri();?>/images/common/nav06.png" alt=""></a>
        <a href="<?php echo get_home_url();?>/contact"><img src="<?php echo get_template_directory_uri();?>/images/common/nav07.png" alt=""></a>
      </nav>
    </div>
  </header>
  <?php if(is_home() || is_front_page()):?>
    <section class="mv">
      <?php for($i=1;$i<=2;$i++):?>
        <div class="img"><img src="<?php echo get_template_directory_uri();?>/images/index/mv<?php echo $i;?>.png" alt=""></div>
      <?php endfor;?>
    </section>
  <?php endif;?>
