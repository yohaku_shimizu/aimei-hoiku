<?php include_once("doremi-header.php");?>
<div class="doremi-info doremi">
  <section class="mv">
    <div class="img"><img src="<?php echo get_template_directory_uri();?>/images/doremi/info/mv.png" alt=""></div>
  </section>
  <section class="info-top">
    <div class="wrap10">
      <h2><span>入園案内</span>INFORMATION</h2>
    </div>
  </section>
  <section class="cont1">
    <div class="wrap10">
      <article class="gr-box">
        <div class="left">
          <h3>大切なお子様にとって初めての集団生活です。</h3>
          <p class="text">
            保護者様におかれましては、ご心配なことが多数おありのことと思います。本園では、安心してお預けいただけますよう皆様をお迎えする準備をしております。ご質問などがありましたら何なりとお尋ねください。
          </p>
          <div class="img">
            <img src="<?php echo get_template_directory_uri();?>/images/doremi/info/img1.png" alt="">
          </div>
        </div>
        <div class="right">
          <div class="img"><img src="<?php echo get_template_directory_uri();?>/images/doremi/info/img2.png" alt=""></div>
        </div>
      </article>
    </div>
  </section>
  <section class="cont2">
    <div class="wrap10">
      <dl>
        <dt>令和２年度の募集</dt>
        <dd>募集定員については、園に直接お問い合わせください  </dd>
      </dl>
      <dl>
        <dt>開所時間</dt>
        <dd>
          月～金曜日	 	午前7時～午後7時まで<br>
          土曜日	 	      午前7時～午後6時まで<br>
          尚、一号子どもさんの保育時間は午前9時30分～午後3時までです。<br>
          <span>※延長保育(午後6時～午後7時)は、別に費用が必要です。</span>
        </dd>
      </dl>
      <dl>
        <dt>利用料</dt>
        <dd>
          利用料は無償です。
        </dd>
      </dl>
      <dl>
        <dt>諸費用について</dt>
        <dd>主食費、服飾費、その他は実費徴収です。</dd>
      </dl>
      <dl>
        <dt>入園までに</dt>
        <dd>
          ・用品（制服など）の注文<br>
          ・教材の購入<br>
          ・健康診断<br>
          ・親子面談<br>
          等があります。
        </dd>
      </dl>
      <dl>
        <dt>2号3号認定こども<br>入園申込みについて</dt>
        <dd>
          入園受付窓口はお住まいの区の区役所です。<br>
          詳しくは、区役所民生子ども課へお問い合わせください。<br>
           <br>
          平成31年度　利用申込み手続き～入所の承諾までの流れ<br>
          10月1日～	…	利用申込書配布（区役所・各保育園）<br>
          10月15日～12月10日	…	利用申込み受付期間<br>
          (12月10日申込み締め切り)<br>
          平成31年2月上旬	…	利用の承諾・不承諾通知の送付
        </dd>
      </dl>
      <dl>
        <dt>満３歳児<br>入園申込みについて</dt>
        <dd>入園希望の方は直接名古屋ドレミ保育園まで電話ください</dd>
      </dl>
      <dl>
        <dt>区役所受付面接日</dt>
        <dd>
          日時　平成30年10月23日(火)<br>
          受付時間　13:30～16:00 <br>
          場所　名古屋ドレミ保育園	 2階 遊戯室<br>
          持ち物<br>
          ・保育所利用申込書<br>
          ・家庭でお子さんの保育ができない状況を確認できる書類<br>
          ・印鑑<br>
          <span>◆当日は入所されるお子様を連れて、保育園までお越しください。</span>
        </dd>
      </dl>
    </div>
  </section>
</div>
