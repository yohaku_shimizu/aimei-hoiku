<?php get_header(); ?>
<?php $td = get_template_directory_uri();$page_slug = get_post(get_the_ID())->post_name; $single = get_post_type();?>
<?php
  $parent_id = $post->post_parent;
  if(get_post($parent_id)->post_name && !$parent_id == 0){
    $getSlug = get_post($parent_id)->post_name;
    include_once($getSlug."/".$page_slug.".php");
  }else{
    $getSlug = $page_slug;
    include_once($getSlug."/index.php");
  }
?>
<?php get_footer(); ?>
