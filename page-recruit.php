<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri(); ?>/images/recruit/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap recruit">
    <nav class="page-nav">
      <a href="#cont01">メッセージ</a>
      <!--<a href="#cont02">先輩の声</a>-->
      <a href="#cont03">募集要項</a>
      <a href="#cont04">お問い合わせ</a>
      <a href="" class="dammy" style="opacity:0;"></a>
    </nav>
    <div class="r_bn">
      <!-- <a href="<?php echo get_template_directory_uri(); ?>/pdf/sinseido.pdf" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/recruit/r_bn.jpg" alt="新保育士さん必見"></a> -->
    </div>
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/recruit/h2-01.png" alt=""></h2>
      <!-- <p class="img">
        <img src="<?php echo get_template_directory_uri(); ?>/images/recruit/r-img.png" alt="">
      </p>
      <p class="text">
        愛名保育園は保護者の方からも、アットホームな保育園と言われるくらい子どもたちと、保育士との距離間が近くに感じる保育園です。子どもたち全員の名前や性格だけでなく、誕生日、好きなもの、嫌いなものなどその子の特色を一人ひとりの保育士が把握しています。それは保育士間でのコミュニケーションをとることで園児の性格や特徴を皆で共有することができているからだと思います。また子どもたちの健やかな成長を見守る為に、日々自立を促していき、身の回りのことに自ら進んで行える環境をつくること。毎日適度な運動を行い、四肢の発達を促すこと。遊びを通して友達とのよい関係性を作ること。を意識しています。
        子どもたちにとって人間形成、人格形成が大きく作られていく、この時期をどれだけ保育士が深くかかわれるかを職員会議や研修を通して追及しています。子どもたちの自主性をいかにして伸ばしていくか、又見出す事が出来るのかを大切にしています。
        子どもたちが卒園後、成長した際に、「愛名保育園でよかった。」「あんな保育士さんいたなー」と思い返してもらえるような保育を目指しています。
      </p> -->
      <article class="block">
        <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/recruit/message_01.png" alt=""></p>
        <p class="text">
          日々の保育や職員会議などでも、新人の私達の意見をたくさん取り入れてもらえるので,主体的に保育に取り組むことができます。<br>また、私たちの得意や「もっと学びたい！」の気持ちが活かせる分野に分かれたチーム活動が自分自身のスキルアップに繋がり、努力を認めてくださる温かい職場です。先輩や歳の近い先生方と対話を通してコミュニケーションを楽しくとることができるので、心地よい職場で働きたいと思っている方はぜひ見学に来てください！！
        </p>
      </article>
      <article class="block">
        <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/recruit/message_02.png" alt=""></p>
        <p class="text">
          一人で保育を考えるのではなく、保育者の「思い」を色々な視点から考え丁寧な関わりをして行く為にも、幅広い年齢層での見守りは、子ども達の情緒を育む上でもとてもプラスになる環境と捉えています。老若男女勢揃いの家庭的なメンバーで、個々の保育者の強みを活かした保育をしていく環境が、子ども達の笑顔の支えとなるよう互いの保育を尊重し合い協力し合って保育しています。
        </p>
      </article>
      <article class="block">
        <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/recruit/message_03.png" alt=""></p>
        <p class="text">
          子どもの主体的なあそび(学び)の中から次々と生まれるエピソード。保育者の誰とでも子ども達のエピソードを楽しく語り合う時間を大切にしています。<br>語り合いを通して、それぞれの保育者が自分自身の「思い」や気持ちに気づき、ワクワクドキドキしながら「あそび」を見守る雰囲気があります。子ども達の喜怒哀楽に共感しながら共に学び合いの場となるような保育を心がけています。
        </p>
      </article>
    </section>
    <section class="cont02" id="cont02">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/recruit/h2-02.png" alt=""></h2>
      <ul class="system">
        <li>
          <a href="<?php echo get_template_directory_uri(); ?>/pdf/system_01.pdf" target="_blank">
            <img src="<?php echo get_template_directory_uri(); ?>/images/recruit/btn_01.png">
          </a>
        </li>
        <li>
          <a href="<?php echo get_template_directory_uri(); ?>/pdf/system_04.pdf" target="_blank">
            <img src="<?php echo get_template_directory_uri(); ?>/images/recruit/btn_02.png">
          </a>
        </li>
        <li>
          <a href="<?php echo get_template_directory_uri(); ?>/pdf/system_03.pdf" target="_blank">
            <img src="<?php echo get_template_directory_uri(); ?>/images/recruit/btn_03.png">
          </a>
        </li>
        <li>
          <a href="<?php echo get_template_directory_uri(); ?>/pdf/system_02.pdf" target="_blank">
            <img src="<?php echo get_template_directory_uri(); ?>/images/recruit/btn_04.png">
          </a>
        </li>
      </ul>
    </section>
    <section class="cont03" id="cont03">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/recruit/h2-03.png" alt=""></h2>
      <article class="list">
        <dl>
          <dt>職種</dt>
          <dd>保育教諭</dd>
        </dl>
        <dl>
          <dt>雇用形態</dt>
          <dd>正社員・パート（時間応相談）</dd>
        </dl>
        <dl>
          <dt>募集人数</dt>
          <dd>詳しくは園までお問い合せ下さい。</dd>
        </dl>
        <dl>
          <dt>応募資格</dt>
          <dd>
            学歴: 高校・短大・専門学校卒以上
          </dd>
        </dl>
        <dl>
          <dt>免許・資格</dt>
          <dd>
            幼稚園教諭免許、保育士資格<br>
            上記を取得する予定の学生
          </dd>
        </dl>
        <dl>
          <dt>仕事内容</dt>
          <dd>
            ・園児の教育、保育<br>
            ・保育関連業務
          </dd>
        </dl>
        <dl>
          <dt>勤務地</dt>
          <dd>
            〒455-0001<br>
            名古屋市港区七番町五丁目３番地
          </dd>
        </dl>
        <dl>
          <dt>勤務時間</dt>
          <dd>月の変形労働時間制（早番・遅番あり）</dd>
        </dl>
        <dl>
          <dt>社会保険</dt>
          <dd>健康保険・厚生年金保険・雇用保険・労災保険・退職金制度</dd>
        </dl>
        <dl>
          <dt>選考日</dt>
          <dd>
            日程がお決まり次第お知らせします。<br>
            詳しくは園までお問い合せ下さい。
          </dd>
        </dl>
        <dl>
          <dt>選考場所</dt>
          <dd>愛名保育園</dd>
        </dl>
        <dl>
          <dt>応募書類の締め切り</dt>
          <dd>
            日程がお決まり次第お知らせします。<br>
            詳しくは園までお問い合せ下さい。
          </dd>
        </dl>
        <dl>
          <dt>試験内容</dt>
          <dd>面接の試験を行います。詳しくは園までお問い合せ下さい。</dd>
        </dl>
        <dl>
          <dt>持ち物</dt>
          <dd>
            ・筆記用具 <br>
            ・履歴書（写真貼付）
          </dd>
        </dl>
        <dl>
          <dt>園の見学</dt>
          <dd>
            園見学につきまして、行事等の期間を除き随時受け入れを致しておりますので、詳しくは園までお問い合せ下さい。
          </dd>
        </dl>
        <dl>
          <dt>備考</dt>
          <dd>なし</dd>
        </dl>
      </article>
    </section>
    <section class="cont04" id="cont04">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/recruit/h2-04.png" alt=""></h2>
      <article class="form">
        <?php echo do_shortcode('[contact-form-7 id="28" title="採用"]'); ?>
      </article>
    </section>
  </div>
  <div class="sidebar">
    <?php get_sidebar(); ?>
  </div>
</section>
<?php get_footer(); ?>