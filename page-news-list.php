<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri();?>/images/news/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap news-list">
    <!--<nav class="page-nav">
      <a href="#cont01">お知らせ</a>
      <a href="#cont02">園のブログ</a>
    </nav>-->
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/news/news-h2.png" alt=""></h2>
      <article class="list">
        <?php
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          query_posts( array(
            'post_type'=>'news',
            'posts_per_page' => 6,
            'paged' => $paged,
            )
          );
        ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <?php
          $terms = wp_get_object_terms($post->ID,'news_cat');
          foreach($terms as $term){
            $cat = $term->name;
            $slug = $term->slug;
          }
          ?>
          <a href="<?php echo the_permalink();?>">
            <p class="tag">｜ <?php echo get_the_date("Y.m.d");?> 　<?php echo $cat;?></p>
            <p class="title"><?php echo get_the_title();?></p>
          </a>
      <?php endwhile; else : ?>
      <?php endif; ?>
      <?php wp_reset_query();?>
      </article>
      <a href="<?php echo get_home_url();?>/news" class="more">すべてのお知らせを見る</a>
    </section>
    <!--<section class="cont02" id="cont02">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/news/blog-h2.png" alt=""></h2>
      <article class="list">
        <?php
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          query_posts( array(
            'post_type'=>'blog',
            'posts_per_page' => 6,
            'paged' => $paged,
            )
          );
        ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <?php
          $terms = wp_get_object_terms($post->ID,'blog_cat');
          foreach($terms as $term){
            $cat = $term->name;
            $slug = $term->slug;
          }
          ?>
          <a href="<?php echo the_permalink();?>">
            <p class="img">
              <?php if(get_field("thumbs")):?>
                <img src="<?php the_field("thumbs");?>" alt="">
              <?php else:?>
                <img src="<?php echo get_template_directory_uri();?>/images/common/white-back.jpg" alt="">
              <?php endif;?>
            </p>
            <p class="title"><?php echo get_the_title();?></p>
            <p class="date"><?php echo get_the_date("Y.m.d");?></p>
          </a>
        <?php endwhile; else : ?>
        <?php endif; ?>
        <?php wp_reset_query();?>
      </article>
      <a href="<?php echo get_home_url();?>/blog" class="more">すべてのブログを見る</a>
    </section>-->
  </div>
  <div class="sidebar">
    <?php get_sidebar();?>
  </div>
</section>
<?php get_footer(); ?>
