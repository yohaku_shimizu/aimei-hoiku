<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri(); ?>/images/about/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap about">
    <section class="midashi">
      <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/about/h2-01.png" alt=""></p>
      <p class="text">
        日常の園の生活でも、給食などの衛生対策に関しても積極的に取り組み、<br class="pc">園の子供たちの健やかな成長のために努めています。
      </p>
    </section>
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/about/h2-02.png" alt=""></h2>
      <article class="flex">
        <p class="left">
          <img src="<?php echo get_template_directory_uri(); ?>/images/about/img06.png" alt="">
        </p>
        <p class="right text">
          <span>園長 日比勇三より</span><br>
          愛名保育園では、『幼児期に友達と遊びこみ、思いやりや協調性、忍耐力などの社会性＝人と関わる力を身につけること』が、最も大切なことだと考えています。子育てに飛び級はありません。各年齢に相応しい遊びや体験を積み重ねて、「いっぽいっぽ」着実に歩みを進めることが、子どもの将来の土台となると確信し、日々の保育に努めています。
        </p>
      </article>
      <p class="text">
        当保育園は、地域の子育て世帯や地域の方に安心して保育園生活を過ごしていただけるように平成27年度より社会福祉法人化をしました。そんな中で私達は、保育の見直しの必要性を感じるようになりました。これまで特に運動会や発表会等の行事に関してはみんなでちょっと頑張って出来るような目標を掲げ取り組んでおりましたが、今の子ども達の様子を見ていると日常的に抱っこやオンブ等を求める子が増え、よって日常の保育も行事も共に頑張ることの手前に「ひとり、ひとりに寄り添い、その子の行動や発言を肯定的に受け止め子ども達と共有していく」ような関わりを丁寧に行っていくことが大切だと感じるようになりました。今までの保育でもやってきたことではありますが、より豊かな表情（笑顔）で「たのしい」「おもしろい」と思える言葉かけを心がけ、ゆったりとした雰囲気と環境の中で誰もが“ここに居ていいんだ”と思え、安心して生活し、自分らしさを育めるような保育を行っていきたいと思うようになりました。そして日々、淡々と繰り返される暮らしの中で子ども達の小さな成長を見逃さず、みんなで喜び合い感動するような場面を大事にしていきたいと思います。<br>
        <br>
        「人を大切にする園運営をする」という目標についても改めて振り返り、子どもがゆったりと安心して過ごすことが出来るためにも職員も、もう少し余裕をもって保育ができるように考えていきたいと思っています。<br>
        <br>
        現状の保育制度の中ではなかなか厳しくいつも壁にぶつかってばかりですが、できればどの職員も自分の生活も大切にしながら一生の仕事として誇りをもって「保育」という“子ども達の成長を共に喜び合える”素晴らしい仕事を続けていけるようになったらいいなぁ…と心から思っています。<br>
        <br>
      </p>
      <div class="precious">
        <div class="precious__inner">
          <img src="<?php echo get_template_directory_uri(); ?>/images/about/about_01.png" alt="">
        </div>
        <!-- <div class="precious__inner sp">
        <p class="title">＜私たちが大切にしていること＞</p>
        <ul class="precious__inner--pic">
          <li>
            <p class="text title">１.分けへだてなく</p>
            <p class="text">愛名保育園の創設者のモットーである「分け隔てなく」の精神を受け継ぎ、子どもにも保護者にも地域にも心配りをしています。どんな子どもも排除されることなく、誰もが助け合って園生活を送ることが出来るよう配慮し、「愛名保育園ならば安心して任せられる」と信頼される保育園を目指しています。</p>
          </li>
          <li>
          <p class="text title">２.異年齢保育</p>
            <p class="text">兄弟の数が減少し、また地域でも大きい子や小さい子が一緒になって遊ぶ機会が減った今、意図的に異年齢保育を実施し、大きい子は小さい子を思いやり、小さい子は大きい子を尊敬する心を育てています。</p>
          </li>
          <li>
          <p class="text title">３.運動あそび</p>
            <p class="text">子どもの脳の発達に良い運動あそびを保育の中で積極的に取り入れています。心・脳・体の育ちを促し、幼児期に大切な運動を好きになって自発的に運動したくなるという気持ちを育てることを大切にしています。</p>
          </li>
        </ul>
      </div> -->
      </div>
    </section>
    <nav class="page-nav">
      <a href="#cont02">園の特徴</a>
      <a href="#cont03">園の目標</a>
      <a href="#cont04">目指す子ども像</a>
      <a href="#cont05">園の歴史</a>
      <a href="#cont06">施設概要</a>
      <a href="#cont07">各種資料</a>
    </nav>
    <section class="cont02" id="cont02">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/about/h2-03.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/about/img01.png" alt=""></p>
      <p class="text">
        子どもの能力を自然に伸ばしていく保育、教育を行っています。<br>
        子どもの脳の発達に良い、運動あそびを保育の中で取り入れています。<br>
        障がい児保育、統合保育を行っています。<br>
        子育て支援事業育児相談や、未就園児を対象とした子育て支援室を開催。<br>
        保育参加、子どもの園での様子を保育参加して見て頂いています。
      </p>
    </section>
    <section class="cont03" id="cont03">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/about/h2-04.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/about/img02.png" alt=""></p>
      <p class="text">
        身近な物事に感謝する心を育てる。<br>
        人との関わりの中で愛情と信頼感を育み、助け合える子を育てる。<br>
        家庭的な雰囲気の中で、子どものさまざまな思いを受けとめ、情緒の安定を図る。<br>
        健康・安全など、生活に必要な基本的な習慣や態度を養い、心身の健康の基礎を身につける。<br>
        身の回りのことについて興味関心を育て、思考力の基礎や創造性の芽生えを培う。<br>
        環境を通して、遊びの中から生きる力の基礎となる自己肯定感(自尊感情)を養う。
      </p>
    </section>
    <section class="cont04" id="cont04">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/about/h2-05.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/about/img03.png" alt=""></p>
      <p class="text">
        自分のことが自分でできる子どもを育てる<br>
        明るく生き生きとした自主性に満ちた子ども<br>
        おもいやりがあり協調性・社会性のある子ども<br>
        独創性に富み、探求する事にじっくり向かって努力できる子ども
      </p>
    </section>
    <section class="cont05" id="cont05">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/about/h2-06.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/about/img04.png" alt=""></p>
      <article class="list">
        <dl>
          <dt>昭和２８年５月１日</dt>
          <dd>
            開園　名称「愛名保育園」<br>
            園長　日比　高則
          </dd>
        </dl>
        <dl>
          <dt>昭和２８年７月１日</dt>
          <dd>社会福祉施設として認可を受ける（定員６０名）</dd>
        </dl>
        <dl>
          <dt>昭和３０年９月１日</dt>
          <dd>園長　日比　静子就任</dd>
        </dl>
        <dl>
          <dt>昭和３７年３月</dt>
          <dd>園舎前面改築完成</dd>
        </dl>
        <dl>
          <dt>昭和４６年４月１日</dt>
          <dd>定員６７人から８０名に変更</dd>
        </dl>
        <dl>
          <dt>昭和４８年６月６日</dt>
          <dd>園長　中山　満寿子就任</dd>
        </dl>
        <dl>
          <dt>昭和５０年４月１日</dt>
          <dd>障がい児保育指定園となる</dd>
        </dl>
        <dl>
          <dt>昭和５７年４月１日</dt>
          <dd>乳児保育の開始とともに定員９０名に変更</dd>
        </dl>
        <dl>
          <dt>平成２７年４月１日</dt>
          <dd>
            社会福祉法人　愛名設立<br>
            園長　日比　勇三就任
          </dd>
        </dl>
        <dl>
          <dt>平成３０年３月３１日</dt>
          <dd>園舎全面改築完成</dd>
        </dl>
      </article>
    </section>
    <section class="cont06" id="cont06">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/about/h2-07.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/about/img05.png" alt=""></p>
      <article class="list2">
        <dl>
          <dt>設置法人</dt>
          <dd>社会福祉法人 愛名</dd>
        </dl>
        <dl>
          <dt>園名</dt>
          <dd>愛名保育園（幼保連携型認定こども園）</dd>
        </dl>
        <dl>
          <dt>園長</dt>
          <dd>日比 勇三</dd>
        </dl>
        <dl>
          <dt>設置認可</dt>
          <dd>昭和28年7月1日</dd>
        </dl>
        <dl>
          <dt>施設概要</dt>
          <dd>
            鉄筋コンクリート造　述べ床面積 917.82㎡<br>
            乳児室０歳児（１F１室）50.88㎡<br>
            乳児室１歳児（１F１室）70.03㎡<br>
            保育室２歳児（２F１室）47.26㎡<br>
            保育室3～5歳児（２F１室）147.90㎡<br>
            ランチルーム（２F１室）56.36㎡<br>
            ホール（３F１室）107.42㎡<br>
            職員室（１F１室）32.76㎡<br>
            保健室（１F１室）5.57㎡<br>
            調理室（１F１室）33.38㎡<br>
            調乳室（１F１室）3.59㎡<br>
            乳児便所（１F１室）7.02㎡<br>
            乳児便所（２F１室）12.43㎡<br>
            幼児便所（２F１室）15.34㎡<br>
            もく浴室（１F１室）7.02㎡<br>
            その他（１F）134.20㎡・（２F）106.94㎡・（３F）79.72㎡
          </dd>
        </dl>
        <dl>
          <dt>所在地</dt>
          <dd>
            〒455-0001 名古屋市港区七番町五丁目３番地
          </dd>
        </dl>
        <dl>
          <dt>連絡先</dt>
          <dd>
            TEL：052-653-6016 FAX：052-653-6029
          </dd>
        </dl>
        <dl>
          <dt>最寄駅</dt>
          <dd>名古屋市営地下鉄名港線 東海通1番出口 徒歩2分</dd>
        </dl>
        <dl>
          <dt>受け入れ年齢</dt>
          <dd>6ヵ月から小学校就学前まで</dd>
        </dl>
        <dl>
          <dt>開所日</dt>
          <dd>月曜日から土曜日まで</dd>
        </dl>
        <dl>
          <dt>開所時間</dt>
          <dd>７時２０分から１９時２０分まで</dd>
        </dl>
        <dl>
          <dt>短時間保育(1号認定)</dt>
          <dd>８時３０分から１６時３０分まで</dd>
        </dl>
        <dl>
          <dt>延長保育</dt>
          <dd>１８時２０分から１９時２０分まで</dd>
        </dl>
        <dl>
          <dt>土曜日</dt>
          <dd>
            ７時２０分から１８時２０分まで<br>
            ＊土曜日の延長保育はありません
          </dd>
        </dl>
        <dl>
          <dt>休所日</dt>
          <dd>
            日曜日、国民の祝日、休日<br>
            ＊12月29日から1月3日は休園日となります
          </dd>
        </dl>
        <!--<dl>
          <dt>決算報告書</dt>
          <dd>
            ・現況報告書 <a href="<?php echo get_template_directory_uri(); ?>/pdf/genjou.pdf" target="_blank"><i class="far fa-file-pdf"></i></a><br>
            ・決算報告書 <a href="<?php echo get_template_directory_uri(); ?>/pdf/kessan.pdf" target="_blank"><i class="far fa-file-pdf"></i></a>
          </dd>
        </dl>-->
        <dl>
          <dt>保護者会会則</dt>
          <dd>
            保護者会会則 <a href="<?php echo get_template_directory_uri(); ?>/pdf/hogosha.pdf" target="_blank"><i class="far fa-file-pdf"></i></a><br>
          </dd>
        </dl>
      </article>
      <article class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3263.6948580077287!2d136.88553301524334!3d35.114329680330364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x600379eb8546f0fb%3A0x7219dd8b421af276!2z44CSNDU1LTAwMDEg5oSb55-l55yM5ZCN5Y-k5bGL5biC5riv5Yy65LiD55Wq55S677yV5LiB55uu77yT!5e0!3m2!1sja!2sjp!4v1518066163806" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        <a href="https://www.google.com/maps?ll=35.11433,136.887722&z=16&t=m&hl=ja&gl=JP&mapclient=embed&q=%E3%80%92455-0001+%E6%84%9B%E7%9F%A5%E7%9C%8C%E5%90%8D%E5%8F%A4%E5%B1%8B%E5%B8%82%E6%B8%AF%E5%8C%BA%E4%B8%83%E7%95%AA%E7%94%BA%EF%BC%95%E4%B8%81%E7%9B%AE%EF%BC%93">
          <img src="<?php echo get_template_directory_uri(); ?>/images/about/map-link.png" alt="">
        </a>
      </article>
    </section>
    <section class="cont07" id="cont07">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/about/h2-08.png" alt=""></h2>
      <p class="text">
        様々な提出書類書式をダウンロード・プリントアウトして提出できます。<br>
        どうぞご活用ください。
      </p>
      <article class="block">
        <div class="flex">
          <p class="left">
            お医者さんから処方された薬を持参される場合は、お薬表を印刷し必要事項を記入の上、薬と一緒に直接職員にお渡しください。
          </p>
          <div class="right">
            <a href="<?php echo get_template_directory_uri(); ?>/pdf/kusuri.pdf" target="_blank">お薬表</a>
            <a href="<?php echo get_template_directory_uri(); ?>/pdf/kusuri-kana.pdf" target="_blank">おくすりひょう（ルビあり）</a>
          </div>
        </div>
        <div class="flex">
          <p class="left">
            土曜日希望保育利用申請書は、​毎月前月２５日までに翌月1ヵ月分の申込お願いします。
          </p>
          <div class="right">
            <a href="<?php echo get_template_directory_uri(); ?>/pdf/donichi.pdf" target="_blank">土曜日保育申請書</a>
            <a href="<?php echo get_template_directory_uri(); ?>/pdf/donichi-kana.pdf" target="_blank">どようびほいくもうしこみしょ（ルビあり）</a>
          </div>
        </div>
      </article>
    </section>
  </div>
  <div class="sidebar">
    <?php get_sidebar(); ?>
  </div>
</section>
<?php get_footer(); ?>