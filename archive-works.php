<?php get_header(); ?>
<?php $td = get_template_directory_uri();?>
<div class="archive-works under">
  <section class="page-title">
    <div class="wrap10">
      <h2>Works<span>/実績紹介</span></h2>
    </div>
  </section>
  <section class="cont1">
    <div class="wrap128">
      <article class="search-box">
        <?php echo do_shortcode('[searchandfilter id="39"]');?>
      </article>
      <article class="single-wrap">
        <?php
          $paged = get_query_var('paged') ? get_query_var('paged') : 1;
          $args = array(
            'post_type' => 'works', // カスタム投稿タイプ Products
            'posts_per_page' => 10,
            'paged' => $paged,
            'post_status' => 'publish',
          );
          if(!$_GET['_sfm_year'] == ""){
            $setMonth=$_GET['_sfm_year'];
            $args['year']=$setMonth;
          }
          if(!$_GET['_sft_works_cat1'] == ""){
            $categorys = $_GET['_sft_works_cat1'];
            $taxquerys = array(
              array(
                'taxonomy' => 'works_cat1', // カスタム分類 Products-cat
                'field' => 'slug',
                'terms' => $categorys, // ターム item1 で絞り込む
              )
            );
            $args['tax_query'] = $taxquerys;
          }
          $the_query = new WP_Query($args); if($the_query->have_posts()):
        ?>
        <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
          <?php
            $terms1 = wp_get_object_terms($post->ID,'works_cat1');
            foreach($terms1 as $term){
              $cat1 = $term->name;
            }
            $terms2 = wp_get_object_terms($post->ID,'works_cat2');
            foreach($terms2 as $term){
              $cat2 = $term->name;
            }
          ?>
          <div class="single">
            <div class="inner">
              <p class="cat"><span><?php echo $cat1;?></span> | <?php echo $cat2;?></p>
              <p class="thumbs"><img src="<?php the_field('thumbs');?>" alt=""></p>
              <h3><?php echo get_the_title();?></h3>
              <p class="text">【クライアント】　<?php the_field("client");?></p>
              <p class="text">【部数】　<?php the_field("many");?></p>
              <p class="links"><a href="<?php echo get_permalink();?>">more...</a></p>
            </div>
          </div>
        <?php endwhile; endif;?>
      </article>
    </div>
  </section>
</div>
<?php get_footer(); ?>
