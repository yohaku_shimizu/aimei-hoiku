<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri();?>/images/company/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap company">
    <nav class="page-nav">
      <a href="#cont01">法人情報</a>
      <a href="#cont02">決算資料</a>
    </nav>
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/company/h2-01.png" alt=""></h2>
      <article class="list2">
        <dl>
          <dt>設置法人</dt>
          <dd>社会福祉法人 愛名</dd>
        </dl>
      </article>
      <article class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3263.6948580077287!2d136.88553301524334!3d35.114329680330364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x600379eb8546f0fb%3A0x7219dd8b421af276!2z44CSNDU1LTAwMDEg5oSb55-l55yM5ZCN5Y-k5bGL5biC5riv5Yy65LiD55Wq55S677yV5LiB55uu77yT!5e0!3m2!1sja!2sjp!4v1518066163806" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        <a href="https://www.google.com/maps?ll=35.11433,136.887722&z=16&t=m&hl=ja&gl=JP&mapclient=embed&q=%E3%80%92455-0001+%E6%84%9B%E7%9F%A5%E7%9C%8C%E5%90%8D%E5%8F%A4%E5%B1%8B%E5%B8%82%E6%B8%AF%E5%8C%BA%E4%B8%83%E7%95%AA%E7%94%BA%EF%BC%95%E4%B8%81%E7%9B%AE%EF%BC%93">
          <img src="<?php echo get_template_directory_uri();?>/images/company/map-link.png" alt="">
        </a>
      </article>
    </section>
    <section class="cont07" id="cont07">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/about/h2-08.png" alt=""></h2>
      <p class="text">
        様々な提出書類書式をダウンロード・プリントアウトして提出できます。<br>
        どうぞご活用ください。
      </p>
      <article class="block">
        <div class="flex">
          <p class="left">
            お医者さんから処方された薬を持参される場合は、お薬表を印刷し必要事項を記入の上、薬と一緒に直接職員にお渡しください。
          </p>
          <div class="right">
            <a href="<?php echo get_template_directory_uri();?>/pdf/kusuri.pdf" target="_blank">お薬表</a>
            <a href="<?php echo get_template_directory_uri();?>/pdf/kusuri-kana.pdf" target="_blank">おくすりひょう（ルビあり）</a>
          </div>
        </div>
        <div class="flex">
          <p class="left">
            土曜日希望保育利用申請書は、​毎月前月２５日までに翌月1ヵ月分の申込お願いします。
          </p>
          <div class="right">
            <a href="<?php echo get_template_directory_uri();?>/pdf/donichi.pdf" target="_blank">土曜日保育申請書</a>
            <a href="<?php echo get_template_directory_uri();?>/pdf/donichi-kana.pdf" target="_blank">どようびほいくもうしこみしょ（ルビあり）</a>
          </div>
        </div>
      </article>
    </section>
  </div>
  <div class="sidebar">
    <?php get_sidebar();?>
  </div>
</section>
<?php get_footer(); ?>
