$("section.return a").click(function(){
  $("body,html").animate({scrollTop:0}, 1000);
  return false;
});
$("a[href^='#']").click(function(){
  var tp = $(this).attr('href');
  var mp = $(tp).offset().top - 50;
  $("body,html").animate({scrollTop:mp},1000);
  return false;
});

$(window).scroll(function(){
  var st = $(this).scrollTop();
  var wh = $("header.index").height() + 150;
  if(st >= wh){
    $(".index-nav-wrap").addClass('nav-fix');
  }else{
    $(".index-nav-wrap").removeClass('nav-fix');
  }
});

$(window).on("load resize",function(){
  var ww = $(this).width();
  if(ww >= 1200){
    $(".t-button").removeClass('t-button-act');
    $(".sp-nav-wrap").removeClass('sp-nav-wrap-act');
  }
});
$(".t-button").click(function(){
  $(".t-button").toggleClass('t-button-act');
  $(".sp-nav-wrap").toggleClass('sp-nav-wrap-act');
});
