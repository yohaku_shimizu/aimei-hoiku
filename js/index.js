$(function() {
    $(".mv").slick({
        centerMode: true,
        centerPadding: '30px',
        variableWidth: true,
        slidesToShow: 1,
        dots: true,
        autoplay: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                centerMode: false,
                variableWidth: false,
            }
        }]
    });
});

$(window).on("load resize", function() {
    var ww = $(".mv").width() / 2;
    var rp = ww - 640;
    var lp = rp;
    if (ww >= 640) {
        $(".slick-prev").css("left", lp);
        $(".slick-next").css("right", rp);
    } else {
        $(".slick-prev").css("left", "0");
        $(".slick-next").css("right", "0");
    }
});