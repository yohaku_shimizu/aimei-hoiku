<?php get_header(); ?>
<?php $td = get_template_directory_uri();?>
<div class="samp under working">
  <section class="page-title">
    <div class="wrap10">
      <h2>Sampling<span>/サンプリング</span></h2>
    </div>
  </section>
  <section class="mv">
    <div class="wrap10">
      <p class="img"><img src="<?php echo $td;?>/images/samp/mv.png" alt=""></p>
      <article class="flex">
        <h3>Summary<span>概要</span></h3>
        <p class="text">
          企業様がサンプリングしたい商品・サービスを、指定したエリアの幼稚園・保育園・高齢者施設の先生や責任者から園児、高齢者とその家族にほぼ100%お届けできます。より効率的にサンプルを配布したいと考えている企業様は、ぜひリベラのサンプリングをご検討ください。
        </p>
      </article>
    </div>
  </section>
  <section class="cont1">
    <div class="wrap128">
      <article v-for="(cont,index) in content" class="single" :class='"single" + index'>
        <div class="text-box" :style="{background:'url(<?php echo $td;?>/images/common/back' + (index + 1) +'.png) no-repeat'}">
          <h3>#0{{(index + 1)}}<span>{{cont.subtitle}}</span></h3>
          <p class="text" v-html="cont.text"></p>
        </div>
        <p class="img"><img :src="'<?php echo $td;?>/images/samp/img' + (index + 1) +'.png'" alt=""></p>
      </article>
    </div>
  </section>
  <section class="cont2">
    <div class="wrap128">
      <article class="img">
        <p class="img"><img src="<?php echo $td;?>/images/samp/img4.png" alt=""></p>
        <p class="img"><img src="<?php echo $td;?>/images/samp/img5.png" alt=""></p>
      </article>
      <article class="flex">
        <div class="left">
          <h3>Works<span>サンプリング実績</span></h3>
        </div>
        <div class="right">
          <dl v-for="(list,index) in works">
            <dt>{{list.left}}</dt>
            <dd v-html="list.right"></dd>
          </dl>
        </div>
      </article>
      <a href="<?php echo get_home_url();?>/works" class="more">View More</a>
    </div>
  </section>
</div>
<?php get_footer(); ?>
