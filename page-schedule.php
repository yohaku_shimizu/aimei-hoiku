<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri(); ?>/images/schedule/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap schedule">
    <nav class="page-nav">
      <a href="#cont01">一日の流れ</a>
      <a href="#cont02">年間行事</a>
      <a href="#cont03">月間行事</a>
      <a href="#cont04">給食</a>
    </nav>
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/h2-01.png" alt=""></h2>
      <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/img01.png" alt=""></p>
      <article class="flex">
        <div class="left">
          <dl>
            <dt>時間</dt>
            <dd>0,1,2歳児</dd>
          </dl>
          <dl>
            <dt>07:20</dt>
            <dd>
              順次登園・挨拶・視診<br>
              自由遊び
            </dd>
          </dl>
          <dl>
            <dt>09:00</dt>
            <dd>
              おやつ
            </dd>
          </dl>
          <dl>
            <dt>10:00</dt>
            <dd>
              自由遊び・散歩・リトミック
            </dd>
          </dl>
          <dl>
            <dt>11:00</dt>
            <dd>順次給食</dd>
          </dl>
          <dl>
            <dt>12:00</dt>
            <dd>午睡</dd>
          </dl>
          <dl>
            <dt>14:15</dt>
            <dd>おやつ</dd>
          </dl>
          <dl>
            <dt>14:45</dt>
            <dd>帰りの準備(2歳児のみ)</dd>
          </dl>
          <dl>
            <dt>15:00</dt>
            <dd>順次降園、自由遊び</dd>
          </dl>
          <dl>
            <dt>18:20</dt>
            <dd>延長保育</dd>
          </dl>
          <dl>
            <dt>19:20</dt>
            <dd>延長保育終了</dd>
          </dl>
        </div>
        <div class="right">
          <dl>
            <dt>時間</dt>
            <dd>3,4,5歳児</dd>
          </dl>
          <dl>
            <dt>07:20</dt>
            <dd>
              順次登園・挨拶・視診<br>
              自由遊び
            </dd>
          </dl>
          <dl>
            <dt>10:00</dt>
            <dd>
              (年齢別活動・異年齢活動）<br>
              戸外遊び・散歩・運動遊び・リトミック・コーナー保育(絵画・制作・ごっこ遊び・積み木)
            </dd>
          </dl>
          <dl>
            <dt>11:00</dt>
            <dd>順次給食</dd>
          </dl>
          <dl>
            <dt>12:15</dt>
            <dd>
              自由遊び
            </dd>
          </dl>
          <dl>
            <dt>14:00</dt>
            <dd>帰りの準備</dd>
          </dl>
          <dl>
            <dt>14:15</dt>
            <dd>おやつ</dd>
          </dl>
          <dl>
            <dt>15:00</dt>
            <dd>
              お話し会・順次降園、自由遊び
            </dd>
          </dl>
          <dl>
            <dt>18:20</dt>
            <dd>延長保育</dd>
          </dl>
          <dl>
            <dt>19:20</dt>
            <dd>延長保育終了</dd>
          </dl>
        </div>
      </article>
    </section>
    <section class="cont02" id="cont02">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/h2-02.png" alt=""></h2>
      <article class="box">
        <ul>
          <li>
            <p class="left">4</p>
            <p class="right">
              ・入園式　・進級式
            </p>
          </li>
          <li>
            <p class="left">5</p>
            <p class="right">
              ・園庭整備(ワークショップ)<br>
              ・こどもの日お祝い・親子遠足<br>
              ・さつま芋の苗植え(ぞう組、きりん組)
            </p>
          </li>
          <li>
            <p class="left">6</p>
            <p class="right">
              ・歯科検診・歯みがき指導<br>
              ・内科検診（第１回）<br>
              <!-- ・お魚教室<br> -->
              ・ミニ遠足
            </p>
          </li>
          <li>
            <p class="left">7</p>
            <p class="right">
              ・七夕会<br>
              ・プールびらき<br>
              ・お泊り保育（年長児）
            </p>
          </li>
          <li>
            <p class="left">8</p>
            <p class="right">
              ・こどもおぢばがえり（年長児希望者）<br>
              ・夏季保育期間有（２週間ほど）
            </p>
          </li>
          <li>
            <p class="left">9</p>
            <p class="right">
              ・目的別遠足<br>
              ・老人ホーム訪問（虹の里）
            </p>
          </li>
          <li>
            <p class="left">10</p>
            <p class="right">
              ・ファミリーフェスティバル
            </p>
          </li>
          <li>
            <p class="left">11</p>
            <p class="right">
              ・さつま芋ほり（ぞう組、きりん組）
            </p>
          </li>
          <li>
            <p class="left">12</p>
            <p class="right">
              ・ｸﾘｽﾏｽﾌｪｽﾃｨﾊﾞﾙ<br>・年末年始休有<br>
              ・冬季保育期間有（3日間ほど）
            </p>
          </li>
          <li>
            <p class="left">1</p>
            <p class="right">・もちつき</p>
          </li>
          <li>
            <p class="left">2</p>
            <p class="right">
              ・節分（豆まき）<br>
              ・保育まつり（年長組）<br>
              ・お別れ遠足（年長児）<br>
              ・作品展
              </p>
          </li>
          <li>
            <p class="left">3</p>
            <p class="right">・内科検診（第２回）・お別れ会<br>・卒園式・入学を祝う会（年長児）<br>・春季保育期間有<br>(卒園式後~入園式前まで)</p>
          </li>
        </ul>
      </article>
      <article class="flex2">
        <p class="left">
          令和元年度の年間行事予定表をごらんいただくことができます。
        </p>
        <a href="<?php echo get_template_directory_uri(); ?>/pdf/year-reiwa0.jpg" target="_blank" class="right">
          令和元年度の年間行事予定表
        </a>
      </article>
      <article class="flex3">
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_01.png" alt=""></p>
          <p class="midashi">4月 入園式・進級式</p>
          <p class="text">
            ドキドキ、ワクワクの子どもたち。新しいお友達やクラスに子どもたちも期待で胸が膨らんでいます。
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_02.png" alt=""></p>
          <p class="midashi">5月　園庭・室内整備<br>(ワークショップ)※不定期開催</p>
          <p class="text">
          保護者の方と協力して、園庭・室内ロフトや保育園で使う家具等を制作しています。初めて使う、本格的な工具。子どもたちが出来上がった遊具で、遊ぶ姿想像しながら制作に参加して頂けます。
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_03.png" alt=""></p>
          <p class="midashi">5月 親子遠足</p>
          <p class="text">
          暖かい日差しの中、親子でふれあいの時間を楽しみます。自然豊かな公園や、テーマパークでお弁当を食べたり、ゲームをしたりと、楽しい時間を過ごします。
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_04.png" alt=""></p>
          <p class="midashi">6月 歯磨き指導</p>
          <p class="text">
            歯科衛生士さんが保育園にやってきます。大切な歯を守るため歯の正しい磨き方を教えてくれます。
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_05.png" alt=""></p>
          <p class="midashi">6月 お魚教室</p>
          <p class="text">
          玄関の前におかれた大きな水槽。川魚が季節を通して入れ替わりながら飼育されています。飼育員さんを招き、川に住んでいるお魚のお話をクイズなどで分かりやすく学ぶことが出来ます
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_06.png" alt=""></p>
          <p class="midashi">7月 七夕会</p>
          <p class="text">
          ホールで七夕のお話を聞いた後は、園庭でスイカ割や流しそうめんを楽しみます。
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_07.png" alt=""></p>
          <p class="midashi">7月 お泊り保育</p>
          <p class="text">
            年長さんのお楽しみ行事！お友達と、すごすことで、子どもたちは、ひとまわり、ふたまわりも、たくましくなります！
          </p>
        </div>
        <!-- <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/y22.png" alt=""></p>
          <p class="midashi">7月　夏野菜 栽培</p>
          <p class="text">
            何を育てるか図鑑で調べ自分たちで苗やプランターを買いに行き、暑い夏の日差しの中食育の一環で育てた野菜、苦手な野菜も食べれるようになりました。
          </p>
        </div> -->
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_08.png" alt=""></p>
          <p class="midashi">8月 こどもおぢばがえり</p>
          <p class="text">
            こどもたちは、お母さんお父さんと、遠く離れた奈良県天理市で、様々な行事に参加してとても楽しい2日間を過ごしました！
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_09.png" alt=""></p>
          <p class="midashi">9月 目的制遠足</p>
          <p class="text">
          ４月からの生活を通して興味関心を持ってきたものをさらに深めるために遠足に向かいます。行き先は子どもたちと一緒に話し合って決定します。
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_10.png" alt=""></p>
          <p class="midashi">10月 ﾌｧﾐﾘｰﾌｪｽﾃｨﾊﾞﾙ</p>
          <p class="text">
          ごっこ遊びや絵の具、鬼ごっこなどそのとき流行っている遊びからチームごとに決定します。日頃の遊びに保護者の方も一緒に参加し大盛り上がりです！
          </p>
        </div>
        <!-- <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/y09.png" alt=""></p>
          <p class="midashi">10月 ハロウィンパーティー</p>
          <p class="text">
            毎年恒例のハロウィンパーティー！みんなで仮装したり、お祭りが開かれたりと賑やかな1日です。
          </p>
        </div> -->
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_11.png" alt=""></p>
          <p class="midashi">11月 芋ほり体験</p>
          <p class="text">
            子どもたちが、暑い日差しの中苗を植え、水やりや草抜きをしたお芋。収穫の日は親子で楽しく芋ほりをし、お弁当を食べたり、焼き芋をしたりして、他のしっみます。食育の一環としての行事となっています。　（幼児クラス）
          </p>
        </div>
        <!-- <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/y08.png" alt=""></p>
          <p class="midashi">11月 海洋丸出港式見学</p>
          <p class="text">
            大きな帆船を始めてみる子たちは、大興奮！旗を思い切り振って、出航をお祝いします。年中・年長児
          </p>
        </div> -->
        <!-- <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/y11.png" alt=""></p>
          <p class="midashi">11月 水族館遠足</p>
          <p class="text">
            年中・年長さんが名古屋港水族館に遠足に行きます。どんな生き物にあえるかなぁ？
          </p>
        </div> -->
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_12.png" alt=""></p>
          <p class="midashi">12月 年長児の集い</p>
          <p class="text">
            少しずつ卒園を意識してきた年長さん。着ぐるみショーや、消防団のマーチングバンドを楽しみます。年長児
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_13.png" alt=""></p>
          <p class="midashi">12月 ｸﾘｽﾏｽﾌｪｽﾃｨﾊﾞﾙ</p>
          <p class="text">
            自分たちで考えて作り上げるｸﾘｽﾏｽﾌｪｽﾃｨﾊﾞﾙ。子どもたち一人ひとりの成長がよくわかります。
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_14.png" alt=""></p>
          <p class="midashi">1月 餅つき</p>
          <p class="text">
          石臼と杵を使って本格的なお餅つきを楽しみます。自分たちでついたお餅はもちもちでいつもよりおいしく感じます。
          </p>
        </div>
        <!--<div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/y20.png" alt=""></p>
          <p class="midashi">1月 メリオット体操</p>
          <p class="text">
            TV愛知で有名な、スナメリのメリ夫君があいめい保育園に遊びに来てくれました。子どもたちも大喜びで、ふれあいを楽しみました。
          </p>
        </div>-->
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_15.png" alt=""></p>
          <p class="midashi">2月 豆まき</p>
          <p class="text">
          鬼は外！福は内！の掛け声とともに豆まきを楽しみます。お給食ではお友だちと一緒に恵方巻を作ります。
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_16.png" alt=""></p>
          <p class="midashi">2月 作品展</p>
          <p class="text">
          １年間作りためてきた作品を展示します。作品と共に子どもたちの成長を感じられます。
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_17.png" alt=""></p>
          <p class="midashi">2月 保育祭り</p>
          <p class="text">
            来月卒園を控えた年長児。保育園の思い出を振り返りながら、たくさんのお友達と大切な時間を共有します。年長のみ
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_18.png" alt=""></p>
          <p class="midashi">3月 ドッヂボール大会</p>
          <p class="text">
            運動遊びで練習したドッヂボール。年長児対年中児で真剣勝負です。　年長・年中児
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_19.png" alt=""></p>
          <p class="midashi">3月 お別れ会</p>
          <p class="text">
          卒園児への感謝の気持ちを込めて歌やダンスを披露します。
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_20.png" alt=""></p>
          <p class="midashi">3月 卒園式</p>
          <p class="text">
            毎年3月になるとお別れの時期になる卒園式就学するこども達へ感謝とお別れの意味を込めて<br>年少～年長
          </p>
        </div>
        <div class="single">
          <p class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_21.png" alt=""></p>
          <p class="midashi">3月 入学を祝う会</p>
          <p class="text">
            毎年、卒園式後に保護者会主催で入学を祝う会を行います。<br>卒園児
          </p>
        </div>
        <div class="single dummy" style="opacity:0;">
        </div>
      </article>
    </section>
    <section class="cont03" id="cont03">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/event_22.png" alt=""></h2>
      <p class="text">
        毎月お誕生日会・スイミング・身体測定・避難訓練を実施します
      </p>
      <article class="box">
        <ul>
          <li>
            <div class="left">
              <p class="midashi">食育１</p>
              <p class="text">
              給食やおやつ、本などから実際に作りたい！食べたい！と思う料理を自分たちで作ります。成功だけではなく、失敗することでたくさん学ぶことをしてほしいと思っています。
              子どもたちの「やりたい」気持ちを大切にしているため、新しいものにも挑戦し、いろいろな世界を広げ、心豊かに育ってほしいと考えています。
              </p>
            </div>
            <p class="right"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/img02.png" alt=""></p>
          </li>
          <li>
            <div class="left">
              <p class="midashi">食育2</p>
              <p class="text">
              健康な身体をつくるには「食べることから」です。食事を通して食べることへの意欲を育て、味わって食べる力を身につけてもらいたいと考えています。
              子どもたちが種や苗から野菜を育てます。毎日の水やりや草抜きをし、収穫したときには、自分たちで調理します。
              </p>
            </div>
            <p class="right"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/img03.png" alt=""></p>
          </li>
          <li>
            <div class="left">
              <p class="midashi">運動遊び</p>
              <p class="text">
                合言葉はからだイキイキこころキラキラ！
                たくさん運動をして健康的な身体と心をつくります。
              </p>
            </div>
            <p class="right"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/img04.png" alt=""></p>
          </li>
          <li>
            <div class="left">
              <p class="midashi">お誕生日会</p>
              <p class="text">
              ホールでお友だちのお誕生日をお祝いします。保育者からの出し物を見た後は、お誕生月のお友だちが選んだリクエスト給食を園長先生と一緒に食べます。
              </p>
            </div>
            <p class="right"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/img05.png" alt=""></p>
          </li>
          <li>
            <div class="left">
              <p class="midashi">避難訓練</p>
              <p class="text">
                年間計画を立て、毎月避難訓練を行っています。消防署の方のお話を聞いて防災への意識を高めてます。
              </p>
            </div>
            <p class="right"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/img06.png" alt=""></p>
          </li>
          <li>
            <div class="left">
              <p class="midashi">避難訓練（園外にて）</p>
              <p class="text">
                実際の避難場所へ誘導し非常時に備える訓練。津波を想定して近くの病院まで歩いて避難経路などを確認します。
              </p>
            </div>
            <p class="right"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/img07.png" alt=""></p>
          </li>
          <li>
            <div class="left">
              <p class="midashi">身体測定</p>
              <p class="text">
                ぐんぐん成長する子どもたちの記録を連絡帳に記載します。今月はどれくらい大きくなったかな？
              </p>
            </div>
            <p class="right"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/img08.png" alt=""></p>
          </li>
          <li>
            <div class="left">
              <p class="midashi">スイミング</p>
              <p class="text">
                毎月各週で2回行っています。水になれ、楽しく参加することを目標にしています！年長さんでは飛び込みや、クロールの練習をしている子もいます。
              </p>
            </div>
            <p class="right"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/img09.png" alt=""></p>
          </li>
          <li>
            <div class="left">
              <p class="midashi">リトミック</p>
              <p class="text">
              0歳児からリトミックに参加し、子どもたちの心と体の発達に合わせた歌やリズム遊びを通しながら楽しく体を動かし音楽や音と触れ合います。
              </p>
            </div>
            <p class="right"><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/img11.png" alt=""></p>
          </li>
        </ul>
      </article>
    </section>
    <section class="cont04" id="cont04">
      <h2><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/h2-04.png" alt=""></h2>
      <article class="flex">
        <div class="left">
          <p class="text">
            当園では最新調理機器を備えた調理室で、調理スタッフ(管理栄養士&栄養士)が給食の献立の作成、調理し、各クラスに届けます。食器も陶磁器を使用することにより家庭の環境に近づけています。楽しく美味しく食べることをモットーに1年を通して食への興味と感謝の気持ちを養い、食事のマナーや箸の持ち方を学び、いろんな「食育」を実践しています。
            <br>
            <br>
            〇「なかよし給食」を取り入れています。食物アレルギーのある子どもたちもみんな一緒に食べてもらいたいという思いがあり、卵・乳製品・その他食物アレルギーを除去した給食になります。<br>
            〇0歳児のお子さんには、ミルク・離乳食を月齢や個人差、体調に応じて進めていきます。
          </p>
        </div>
        <div class="img">
          <ul class="slide">
            <li><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/slide07.png" alt=""></li>
            <li><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/slide08.png" alt=""></li>
            <li><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/slide01.png" alt=""></li>
            <li><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/slide02.png" alt=""></li>
            <li><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/slide03.png" alt=""></li>
            <li><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/slide04.png" alt=""></li>
            <!-- <li><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/slide05.png" alt=""></li>
            <li><img src="<?php echo get_template_directory_uri(); ?>/images/schedule/slide06.png" alt=""></li> -->
          </ul>
          <!-- <a href="http://blog.livedoor.jp/aimeihoikuen/" target="_blank" class="link">詳しくは園のブログをご覧ください</a> -->
        </div>
      </article>
    </section>
  </div>
  <div class="sidebar">
    <?php get_sidebar(); ?>
  </div>
</section>
<?php get_footer(); ?>