<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri();?>/images/info/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap information">
    <nav class="page-nav">
      <a href="#cont01">入園までの流れ</a>
      <a href="#cont02">各種資料</a>
    </nav>
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/info/h2-01.png" alt=""></h2>
      <p class="text2">
        幼稚園利用（1号認定）、保育園利用、いずれの場合も、<br>
        最初に園までお問い合わせください。その後以下の手続きをお願いします。
      </p>
      <article class="block">
        <h3>STEP1</h3>
        <p class="text">
          区役所「民生子ども係」で申請必要書類をもらってください<br>
          ※当園にも申請書類があります。
        </p>
      </article>
      <article class="block">
        <h3>STEP2</h3>
        <p class="text">
          書類を参考に希望する園をお選びください<br>
          ※ご不明な点や不安な事は、お気軽にご相談ください。
        </p>
      </article>
      <article class="block">
        <h3>STEP3</h3>
        <p class="text">
          希望する園を見学できます<br>
          ※個別に連絡後、訪問・見学してください。
        </p>
      </article>
      <article class="block">
        <h3>STEP4</h3>
        <p class="text">
          申請書類を区役所「民生子ども係」に提出してください<br>
          ※4月入園の場合、12月中旬締切です。
        </p>
      </article>
      <article class="block">
        <h3>STEP5</h3>
        <p class="text">
          入園承諾通知が来ます（2月初めから中旬頃）<br>
          ※お住まいの区役所より、直接郵送されます。
        </p>
      </article>
      <article class="block">
        <h3>STEP6</h3>
        <p class="text">
          入園決定
          ※4月入園の場合、入園説明会の実施要項をお知らせいたします。 <br>
          途中入園の場合は個別にご説明いたします。
        </p>
      </article>
      <article class="block">
        <h3>STEP7</h3>
        <p class="text">
          当園の嘱託医医院にて健康診断を受けてください。<br>
          ※保育初日より、約2週間の「慣らし保育」を実施いたします。
        </p>
      </article>
      <p class="text">
        ※詳しくはお住まいの区役所「民生子ども係」まで。
      </p>
      <article class="link-block">
        <p class="text">保育料については、次のとおりとなります。</p>
        <div class="flex">
          <a href="<?php echo get_template_directory_uri();?>/pdf/3riyouryou.pdf" target="_blank">1号認定・2号認定<br>3歳児から5歳児向け</a>
          <a href="<?php echo get_template_directory_uri();?>/pdf/0riyouryou.pdf" target="_blank">2・3号認定<br>0歳児から2歳児向け</a>
        </div>
      </article>
    </section>
    <section class="cont02" id="cont02">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/info/h2-02.png" alt=""></h2>
      <article class="block">
        <div class="flex">
          <p class="left">こちらに園のパンフレットを載せますので、ご確認くださいませ。</p>
          <a href="<?php echo get_template_directory_uri();?>/pdf/pamphlet.pdf" class="right" target="_blank">園のパンフレット</a>
        </div>
        <div class="flex">
          <p class="left">令和3年度の入園のしおりをごらんいただくことができます。</p>
          <a href="<?php echo get_template_directory_uri();?>/pdf/shiori_r02.pdf" class="right" target="_blank">入園のしおり</a>
        </div>
        <div class="flex">
          <p class="left">令和3年4月、３歳児の「１号認定」の園児を募集します。</p>
          <a href="<?php echo get_template_directory_uri();?>/pdf/r3shousai.pdf" class="right" target="_blank">「１号認定」園児募集要項の詳細</a>
        </div>
      </article>
    </section>
  </div>
  <div class="sidebar">
    <?php get_sidebar();?>
  </div>
</section>
<?php get_footer(); ?>
