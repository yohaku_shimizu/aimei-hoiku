<?php get_header(); ?>
<section class="sv">
  <div class="wrap12">
    <img src="<?php echo get_template_directory_uri();?>/images/news/sv.png" alt="">
  </div>
</section>
<section class="wrap12 side-wrap">
  <div class="main-wrap news-list">
    <section class="cont01" id="cont01">
      <h2><img src="<?php echo get_template_directory_uri();?>/images/news/news-h2.png" alt=""></h2>
      <article class="list">

        <?php
        $term_id = get_queried_object_id(); // タームIDの取得
        $terms = get_terms( 'news_cat');
        foreach ( $terms as $term ){
          if ($term->term_id == $term_id ) {
            $term_name =  $term->slug; //タームのリンク
          }
        }
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
          'paged' => $paged,
          'posts_per_page' => 12,
          'post_type'=>'news',
          'tax_query' => array(
            array(
              'taxonomy' => 'news_cat', // カスタム分類 Products-cat
              'field' => 'slug',
              'terms' => $term_name, // ターム item1 で絞り込む
            )
          )
        );
        $wp_query_post = new WP_Query($args);
        if ($wp_query_post->have_posts()): while($wp_query_post->have_posts()): $wp_query_post->the_post();
        ?>
        <?php
        $terms = wp_get_object_terms($post->ID,'news_cat');
        foreach($terms as $term){
          $cat = $term->name;
          $slug = $term->slug;
        }
        ?>
        <a href="<?php echo the_permalink();?>">
          <p class="tag">｜ <?php echo get_the_date("Y.m.d");?> 　<?php echo $cat;?></p>
          <p class="title"><?php echo get_the_title();?></p>
        </a>
      <?php endwhile; endif; wp_reset_postdata(); ?>

      <!-- =========================================================================================== -->
      <!-- 2020/12/18 記述が違うのでコメントアウトして ↑ に書き直しました 清水 -->
      <!-- <?php
      $tarms = get_the_terms( $post -> ID ,'news_cat' );
      foreach ($tarms as $tarm) {
        $tarmname = $tarm -> slug;
      }
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
      query_posts( array(
        'post_type'=>'news',
        'posts_per_page' => 12,
        'paged' => $paged,
        'tax_query' => array(
          array(
            'taxonomy' => 'news_cat', // カスタム分類 Products-cat
            'field' => 'slug',
            'terms' => $tarmname, // ターム item1 で絞り込む
          )
        )
      )
    );
    ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php
      $terms = wp_get_object_terms($post->ID,'news_cat');
      foreach($terms as $term){
        $cat = $term->name;
        $slug = $term->slug;
      }
      ?>
      <a href="<?php echo the_permalink();?>">
        <p class="tag">｜ <?php echo get_the_date("Y.m.d");?> 　<?php echo $cat;?></p>
        <p class="title"><?php echo get_the_title();?></p>
      </a>
    <?php endwhile; else : ?>
    <?php endif; ?>
    <?php
    if(function_exists('wp_pagenavi')) { wp_pagenavi(); }
    wp_reset_query();
    ?> -->

    <!-- =========================================================================================== -->

  </article>
</section>
</div>
<div class="sidebar">
  <?php get_sidebar();?>
</div>
</section>
<?php get_footer(); ?>
