<?php get_header(); ?>
<section class="wrap12 side-wrap">
  <div class="main-wrap single-post">
    <article class="top">
      <?php
      $terms = wp_get_object_terms($post->ID,'blog_cat');
      foreach($terms as $term){
        $cat = $term->name;
        $slug = $term->slug;
      }
      ?>
      <h2><?php echo get_the_title();?></h2>
      <p class="tag">| <?php echo get_the_date("Y.m.d");?> <?php echo $cat;?></p>
    </article>
    <article class="content-wrap">
      <?php
      $this_content= wpautop($post->post_content);
      echo $this_content;
      ?>
    </article>
  </div>
  <div class="sidebar">
    <?php get_sidebar();?>
  </div>
</section>
<?php get_footer(); ?>
