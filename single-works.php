<?php get_header(); ?>
<?php $td = get_template_directory_uri();?>
<div class="single-works under">
  <section class="page-title">
    <div class="wrap10">
      <h2>News<span>/新着情報</span></h2>
    </div>
  </section>
  <section class="cont1">
    <div class="wrap128">
      <article class="img-flex">
        <p class="img"><img src="<?php the_field('images1');?>" alt=""></p>
        <p class="img"><img src="<?php the_field('images2');?>" alt=""></p>
      </article>
      <article class="text-flex">
        <div class="left">
          <?php
            $terms1 = wp_get_object_terms($post->ID,'works_cat1');
            foreach($terms1 as $term){
              $cat1 = $term->name;
            }
            $terms2 = wp_get_object_terms($post->ID,'works_cat2');
            foreach($terms2 as $term){
              $cat2 = $term->name;
            }
          ?>
          <p class="cat"><span><?php echo $cat1;?></span> | <?php echo $cat2;?></p>
          <p class="title"><?php echo get_the_title();?></p>
          <p class="text">【クライアント】　<?php the_field("client");?></p>
          <p class="text">【部数】　<?php the_field("many");?></p>
        </div>
        <div class="right">
          <dl>
            <dt>案件詳細</dt>
            <dd><?php the_field('text1');?></dd>
          </dl>
          <dl>
            <dt>目的</dt>
            <dd><?php the_field('text2');?></dd>
          </dl>
          <dl>
            <dt>対象</dt>
            <dd><?php the_field('text3');?></dd>
          </dl>
        </div>
      </article>
      <article class="img">
        <div class="wrap10">
          <?php for($i=3;$i<=5;$i++):?>
            <?php if(get_field('images'.$i)):?>
              <p class="img"><img src="<?php the_field('images'.$i);?>" alt=""></p>
            <?php endif;?>
          <?php endfor;?>
        </div>
      </article>
    </div>
  </section>
</div>
<?php get_footer(); ?>
